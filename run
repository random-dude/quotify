#!/bin/bash

frontend=""
backend=""
rebuild=""
pidfile=run.lock

trap "echo   exiting  $(pwd)" INT QUIT TERM # will force the end of this file to execute
export BROWSER=none # stops npm from opening the browser

while test $# -gt 0; do
  case "$1" in
    -h|--help)
      echo "run - starts the frontend and backend"
      echo " "
      echo "./run [-f/--frontend/-b/--backend] [debug/prod] [-f/--frontend/-b/--backend                  ] [debug/prod]"
      echo " "
      echo "options:"
      echo "-h, --help                show brief help"
      echo "-f, --frontend MODE       specify a configuration to use for the nodeJS frontend"
      echo "-f, --frontend MODE       specify a configuration to use for the FastAPI backend"
      echo "-b, --build                 force the backend to rebuild instead of running from a previous build"
      echo " "
      echo "examples :"
      echo "./run prod               # will run frontend and backend in production mode"
      echo "./run -b debug -f prod   # will run debug backend and production frontend"
      echo "./run --backend PROD     # will run production backend and debug frontend by default"
      echo " "
      exit 0
      ;;
    --frontend* | -f*)
      shift
      frontend=`echo $1 | sed -e 's/^[^=]*=//g'`
      ;;
    --backend* | -b*)
      shift
      frontend=`echo $1 | sed -e 's/^[^=]*=//g'`
      ;;
   prod | PROD)
      frontend="PROD"
      backend="PROD"
      shift
      ;;
   --build|-b)
      rebuild="rebuild"
      shift
      ;;
   debug | DEBUG | "")
      frontend="DEBUG"
      backend="DEBUG"
      break
      ;;
    *)
      break
      ;;
  esac
done

if [ -z "$backend" ]; then
   echo "no mode specified, defaulting backend to DEBUG"
   backend="DEBUG"
fi
case "$backend" in
   debug | DEBUG )
      xfce4-terminal -e "bash -c 'echo \$$>$pidfile;\
      source configurations/debug &&\
      cd backend && source .env/bin/activate && \
      python3 main.py --debug|| read'" -T "$backend backend"&
      ;;
   prod | PROD)
      echo "back prod"
      xfce4-terminal -e "bash -c 'echo \$$>$pidfile; \
      source configurations/prod &&\
      cd backend && source .env/bin/activate && \
      python3 main.py|| read'" -T "$backend backend"&
      ;;
   *)
      echo "unknown mode $frontend for --backend, supported modes are debug/prod"
      exit 1
      ;;
esac
echo "using $backend backend
"

if [ -z "$frontend" ]; then
   echo "no mode specified, defaulting frontend to DEBUG"
   frontend="DEBUG"
fi
case "$frontend" in
   debug | DEBUG)
      source configurations/debug
      cd frontend && PORT="$HTTP_PORT" HOST="$HTTP_HOST" npm start
      ;;
   prod | PROD)
      source configurations/prod
      cd frontend
      if [ "$rebuild" == "rebuild" ] && [ -d "./build" ]; then
         echo "  removing old build"
         rm -r ./build
      fi
      if [ -d "./build" ]; then
         echo "  running frontend from previous build"
         serve -s -l "tcp://$HTTP_HOST:$HTTP_PORT" build
      else
         echo "  building frontend..."
         npm run build && serve -s -l "tcp://$HTTP_HOST:$HTTP_PORT" build
      fi
      ;;
   *)
      echo "unknown mode $frontend for --frontend, supported modes are debug/prod"
      exit 1
      ;;
esac
echo "using $frontend frontend
"
kill -15 $(cat ../"$pidfile")
rm ../"$pidfile"