<p align="center">
    <h1 align="center">quotify</h1>
    <h3 align="center"><em>quote generator based on markov chains</em></h3>
</p>
<p align="center">
  <img src="screenshot.png" alt="quotify">
</p>

---
<span style="font-size:1.2em;">
This project uses <a href="https://en.wikipedia.org/wiki/Markov_chain">markov chains </a> to generate pseudo-quotes from books or other textual material.
Multiple sources can be combined to generate an hybrid model.
</span>

---

## what does it do ?

Quotify serves a page allowing the user to select a book from the included library and generate a new quote in the style of the chosen book every time a button is clicked.
The quote can also be heard in the browser thanks to a Text To Speech engine.

If multiples books are selected, a knob allow to tune the weight assigned to each book in the model.

## does the quotes make sense ?

It depends mainly on the book used and on sheer luck. Some books will generate a comprehensive quote one time out of five, some one out of ten... IMHO poetry and highly recognisable texts with unique speech patterns (think of the bible) tends to yield particularly good results but your mileage may vary. Some quotes may be astonishignly believable though.

## how does it work ?

When adding a book, the text is cut into chunks using a neuro linguistic processor to take into account the basic grammar and syntax of the language. The probabilities of a chunk to appear next to another are calculated for each chunk and used to chain them one by one. The first chunk is sampled randomly and the end result is the pseudo-quote.

---

## about quotify

It is based on the the great python library [markovify](https://github.com/jsvine/markovify) by `jsvine`. The frontend is powered by [React](https://en.reactjs.org/) while the API is written in python using [FastAPI](https://fastapi.tiangolo.com/) and served by [uvicorn](https://www.uvicorn.org/)

Text-to-speech capabilities are provided by [picoTTS](https://github.com/naggety/picotts) which can be compiled for ARM plateforms. Alternatively, it is possible to use [gTTS](https://pypi.org/project/gTTS/) which relies on Google Translate and can't be used offline. Text-segmentation uses [spacy](https://spacy.io/) which provides various models for most languages.

This project will run smoothly even on modest servers (tested on raspberry pi 3B). 

For copyright reasons, only a few selection of (french) public domain text files are provided, but it will work with most ebooks converted to `txt` files.

---

## usage

The provided `run` script will start both the frontend and the backend in `debug` mode or `production` mode :
```bash
chmod +x ./run
./run # will run both the back and front into debug mode
./run prod # will run both the back and front into production mode
./run back debug front prod # self-explanatory
./run --help # when in doubt
```

### debug mode
The front-end is run via `npm start` which uses webpack to allow for hot-reloading when a file is modified. The TTS won't work in this mode because an appearing .wav file will cause the page to reload before the audio can be read. So in *debug mode*, the path for the audio files points to the `frontend/build/` directory.

The backend will be served by *uvicorn* with the `--reload` flag set to `True`, allowing for hot-reloading when a python file is updated.

### production mode
The frontend is served by `serve` from Node. It uses the existing compiled `frontend/build` folder or create it if not found. To force the rebuild, simply run `./run back prod build`. The generated build can be hosted with a variety of servers, including *Apache*.

The backend won't allow hot-reloading anymore.

### first start

When the backend is started, every textfile in `backend/corpus` will be pre-computed to a `json` model if this hasn't been already done. *This operation can take up to several minutes*, depending on the size of the corpus, the number of text files and the hardware running it. Meanwhile, the interface will show a loading screen which will automatically updates to the main app once this process ends. Once loaded, the quote generation itself is quite fast.

To avoid the possibly long loading time at the first launch, it is possible to start it locally first and copy every `.json` model created along the text files on the server.


## adding books / source material
Any `txt` file placed in `backend/corpus` will be used at the next launch of the backend. New files will be automatically pre-computed and added to the index file `backend/corpus/booksSummay.json`. This file represents the library of all available books. The *title* and *author* fields can be manually edited to be displayed on the UI. If the `isCurated` flag is set to *false*, the book will *NOT* be included on the library. This is set by default to avoid adding malformed texts.

It is highy recommended to parse the text to remove any chapter mark, footnote... This can be very time consuming and I sadly haven't found a way to automate it entirely for each and every book, but it is worth the effort -hence the `isCurated` flag.

---

## setup (on linux)

### python

You will need a recent version of python3 (>=3.7.x). A `setup` script is provided to install python dependancies into a virtual environnement :

```bash
chmod +x ./setup
./setup
```

### picoTTS

If you wish to use the text-to-speech functionnality, you will need picoTTS. Unfortunately, some linux distributions don't include picoTTS in their package repositories. It is still possible to compile it from source by using the included `build` script. You'll first need to install the popt library ( on fedora/centos/redhat : `sudo dnf install popt-devel` / on debian/ubuntu/mint `sudo apt-get install libpopt-dev` ) as well as the usual C compilation tools.

```bash
chmod +x backend/picotts/build
backend/picotts/build
```

If you are lucky enough to have it in your package manager, just install it and set the `picoTTSpath` variable to `picoTTSpath="pico2wave"` inside `backend/markov.py`

### React

If you are interested in tinkering with the frontend, the simplest way to start is to install [create-react-app](https://reactjs.org/docs/create-a-new-react-app.html#create-react-app) which comes with everything you need to serve and debug the frontend.

If you are simply hosting the project as is, you can automatically build and serve the frontend by using the included `run` script. You will need to install first [npm and Node](https://docs.npmjs.com/downloading-and-installing-node-js-and-npm)

It is of course possible to host the `build` folder by any server of your choice.

---

## setup on raspberry pi

This project can easily be hosted on a raspberry pi, either for remote browsing or as a kiosk. I had good results using Flask to host the frontend, most of the setup being be the same.

However, as the time of writing *spacy* depends on *blis* which is currently broken for ARM plateforms. The installation of *spacy* also require more swap space that is allowed by default. Spacy should be installed separately as follow :

```bash
# enlarging swap to 1024Mo to compile spacy
sed -i -e 's/CONF_SWAPSIZE=100/CONF_SWAPSIZE=1024/g' /etc/dphys-swapfile
/etc/init.d/dphys-swapfile stop && /etc/init.d/dphys-swapfile start
pip3 install spacy==2.0.18 # newer versions depends on blis, currently broken for ARM plateforms
# reverting to default 100Mo swap..."
sed -i -e 's/CONF_SWAPSIZE=1024/CONF_SWAPSIZE=100/g' /etc/dphys-swapfile
/etc/init.d/dphys-swapfile stop && /etc/init.d/dphys-swapfile start