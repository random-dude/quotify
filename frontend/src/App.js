import "./App.css";
import "bootstrap/dist/js/bootstrap";
import "bootstrap/dist/css/bootstrap.min.css";
import "./animations.css"
import {Animate} from "react-animate-mount" //FIXME use CSSTransition instead
import { CSSTransition, SwitchTransition } from 'react-transition-group'
import {BrowserView, isMobile } from "react-device-detect";
import VideoBackground from "./components/videoBackground";
import Corpus from "./components/corpus"
import MainText from "./components/mainText";
import ControlButtons from "./components/controlButtons";
import BookLibraryModal from "./components/bookLibraryModal";
import WaitingScreen from "./components/waitingScreen";
import AudioPlayer from "./components/audioPlayer";
import Tutorial from "./components/tutorial/tutorial";
import About from "./components/about";
import React, {useEffect, useState} from "react";

function App() {

  // ----------------------------- Functions -----------------------------

  // called when closing a book, send to the API the filename of the book closed the refresh the books array
  const removeBook = id => {
    setText("loading"); // will add a spinner while the model is loading
    console.log("removing book", id)
    fetch(APIendpoint + "/removeBookById/"+id, {method: "DELETE"})
    .then(updateBooks)
  }

  // called by a modal selection, gives the API the filename of the book selected then refresh the books array
  // If the modal was triggered from a click on a book's title, the variable bookToReplace won't be null
  const addBook = id =>{
    setText("loading"); // will add a spinner while the model is loading
    if (bookToReplace != null) return swapBooks(bookToReplace, id)
    if (books.filter(b=>{return (b.id===id)}).length >0) return
    console.log("adding book", id)
    fetch(APIendpoint + "/addBookById/"+id, {method: "GET"})
    .then(updateBooks);
  }

  // replace one book by another
  const swapBooks = (toReplace, newBookId) => {
    console.log("replacing book", toReplace, "by", newBookId)
    fetch(APIendpoint+"/replaceBookById", {
      method:"POST",
      headers: { "Content-Type": "application/json" },
      body: JSON.stringify({"toReplace":toReplace, "newBookId":newBookId})
    })
    .then(updateBooks);
  }

  // get the currently used book and set the main text accordingly
  const updateBooks = () => {
    fetch(APIendpoint + "/getCurrentlyUsedBooks", 
      {method:"GET", headers:{ "Content-Type": "application/json" }
    })
    .then(res =>res.json()).then(data=>{
      setBooks(data)
      if (data.length > 0) setText("") // will be replaced in the <Maintext> component
      else setText("choisir un livre dans la bibliothèque pour commencer")
    })
    setAnimateBooks(Math.random());
  }

  // call the API to get a citation, then ask for a TTS render
  const generateSentence = () =>{
    fetch(APIendpoint + "/generateSentence", {method:"GET"})
    .then(res=>res.json()).then(data=>{
      setText(data);
      if (!isMuted) playAudio();
    })
    .catch(console.log)
  }

  // set the audio path then start playing when the fetch promise resolves
  const playAudio = () =>{
    if (books.length === 0) return
    fetch(APIendpoint + "/getAudio", {method:"GET"}).then(res=>res.json()).then(path=>{
      setAudioPath(path, setAudioPlaying(true))
    })
    .catch()
   }

  // try to get the session parameters from the API, retry every 500ms
  // This function is used to determine if the API as finished loading :
  // the loading page will be displayed until a 200 response is received
  const fetchSessionData = () => {
  fetch(APIendpoint + "/getSessionData", {method: "GET"})
    .then(res => {
      if (!res.ok) throw res;
      return res.json();
    })
    .then(data => {
      // no cookies found, the API assigns us a userID (uuidv4)
      if (data.uid !== userID) {
        userID = data.uid;
        setCookie("userID", data.uid);
        setAPIendpoint(API_URL+"/"+data.uid)
      }
      // returning user : books array is already populated by the cookie content
      else if (data.modelNeedsReload) {
        // add each book to the model (they will be ignored by the API if already loaded,
        books.forEach(book => {
          fetch(APIendpoint + "/addBookFromDict", {
            method:"POST",
            headers: { "Content-Type": "application/json" },
            body: JSON.stringify(book)
          })
          // set the weights back to their previous value
          .then(res=> {updateBookWeight(book.id, book.weight); });
        });
      };
      // render the books
      updateBooks();
      // populate the library and hide the waiting screen
      setAvailableBooks(data.availablesBooks);
      if (data.availablesBooks.length >0) showWaitScreen(false)
    })

    // if the server doesn't respond, it might be booting up or loading the books
    // so we'll retry every 500ms while the waiting screen is shown
    .catch(res=>{
      setTimeout(fetchSessionData, 500);
      console.log("unable to fetch session data, retrying in 500ms...",res);
    });
  }

  // update book weight on a wheel/touch/mouse event on the circular-progressbar of a book
  const updateBookWeight = (id, value) =>{
    fetch(APIendpoint + "/updateBookWeight", {
      method:"POST",
      headers: { "Content-Type": "application/json" },
      body: JSON.stringify({"id":id, "value":value})
    })
    // update the books object without calling setBooks which will cause all books to render again
    books.map( b => {if (b.id === id) b.weight = Math.round(value)})
    setCookie("books", books) // the useEffect hook won't be called here since we are avoiding setState
  }

  // toggle TTS on/off and cut the audio off if currently playing
  const toggleMuted = () => {
    if (isAudioPlaying && !isMuted) setAudioPlaying(false);
    setMuted(!isMuted);
  }

  // will show and start the tutorial right away
  const startTutorial = () => {
    setStartTutorialOnLoad(true);
    showTutorial(true);
    // we need to overwrite the opposite cookie that will be automatically stored by the useEffect hook
    setCookie("showTutorial", false)
  }

  // set a cookie for a given variable, including arrays and objects
  const setCookie = (varname, value, expireDays=7, sameSite="Strict", path="/") => {
    let expirationDate = new Date();
    expirationDate.setDate(expirationDate.getDate() + expireDays);
    const cookiesOptions = `expires=${expirationDate.toUTCString()}; path=${path};SameSite=${sameSite};`;
    value = (typeof value === "object" || typeof value === "boolean") ? JSON.stringify(value) : value;
    document.cookie = `${varname}=${value}; ${cookiesOptions}`;
  }

  // get the value of a given variable in the cookies, returns a default value if not set
  const getCookie = (varname, defaultValue=undefined, isJson=false) => {
    varname += "=";
    if (document.cookie.length > 0 && document.cookie.includes(varname)){
      const value = unescape(document.cookie.split(varname).pop().split(";")[0]);
      return (isJson)? JSON.parse(value) : value
    }
    else return defaultValue;
}

// FIXME : remove this debug function
  const clearCookies = () => {
    ["userID", "showTutorial", "isMuted", "books"].forEach(v=>{
      document.cookie = `${v}=0;expires="Sat, 1 Jan 2000 00:00:01 GMT";`;
    });
  };
  document.clearCookies = clearCookies;

// -------------------------- global variables -------------------------

const API_URL = process.env.REACT_APP_API_ENDPOINT;
var userID = getCookie("userID", "newUser");


// ---------------------------- React hooks ----------------------------

const [APIendpoint, setAPIendpoint] = useState(API_URL + "/" + userID );

const [books, setBooks] = useState(getCookie("books", [], true)); // array of books used by the current model
const [availableBooks, setAvailableBooks] = useState([]) // array of curated books used to populate the library modal
const [bookToReplace, setBookToReplace] = useState(null) // if null a modal selection will add a book, if set it will replace this book

const [isAudioPlaying, setAudioPlaying] = useState(false) // will start the audioPlayer
const [audioPath, setAudioPath] = useState("") // TTS wav files are given random filenames to avoid browser caching
const [isMuted, setMuted] = useState(getCookie("isMuted", false, true)); // will enable or disable TTS

const [text, setText] = useState("") // generated citation or instructions
const [isTutorialVisible, showTutorial ] = useState(getCookie("showTutorial", true, true)) // will switch the tutorial ON/OFF
const [startTutorialOnLoad, setStartTutorialOnLoad] = useState(false); // will start the tutorial without needing to click on the button
const [isWaitScreenVisible, showWaitScreen ] = useState(true) // will switch from the loading page to the main app
const [animateBooks, setAnimateBooks] = useState(Math.random()) // getting a new key prop will start the animation on change

useEffect(()=>{setCookie("showTutorial", isTutorialVisible);}, [isTutorialVisible]);
useEffect(()=>{setCookie("isMuted", isMuted);}, [isMuted]);
useEffect(()=>{setCookie("books", books);}, [books]);

  //------------------- static background for mobile --------------------

  if (isMobile) {
    document.body.style.backgroundImage = "url('staticBackground.png')"; 
    import("./components/mobileStaticBackdrop.css")
  }


  // ---------------------------- components ----------------------------

  return (
    <div className="App">

      {/* this page wait for the fetch promise to resolve */}
      <Animate show={isWaitScreenVisible} type="fade">
        <WaitingScreen fetchSessionData={fetchSessionData}/> 
      </Animate>

      <Animate show={!isWaitScreenVisible}>
      {(!isWaitScreenVisible && !isTutorialVisible) && <About startTutorial={startTutorial}/> }
        <BrowserView>
          <VideoBackground filename="raysDK.webm"/>
        </BrowserView>
        { (!isWaitScreenVisible && isTutorialVisible) && 
          <Tutorial
           generateSentence={generateSentence}
           booksLibrary={availableBooks}
           usedBooks={books}
           addBookCB={addBook}
           removeBookCB={removeBook}
           closeTutorial={()=>showTutorial(false)}
           startOnLoad={startTutorialOnLoad}
           />}
        <div className="container-fluid py-1 h-100">

        {/* animated citation */}
        <SwitchTransition>
          {/* we use the text as key to update the animation on text change */}
          <CSSTransition classNames="fade" timeout={200} key={text} >
            <MainText text={text}/>
          </CSSTransition>
        </SwitchTransition>

        <div className="dflex justify-content-around fixed-bottom">

          {/* add-book, mute and generate-citation buttons  */}
          <ControlButtons buttonCB={generateSentence}
                          setBookToReplace={setBookToReplace}
                          isMuted={isMuted}
                          toggleMuted={toggleMuted}
          />

          {/* books and books control */}
          <SwitchTransition>
            <CSSTransition classNames="slide-down" timeout={200} key={animateBooks}>
              <div className="row text-center">
                {books.map((corpus, index) => (
                  <Corpus
                      corpus={corpus}
                      key={index}
                      removeCB={removeBook}
                      adjustWeightCB={updateBookWeight}
                      multipleBooks={books.length > 1}
                      bookToReplaceCB={()=>setBookToReplace(corpus.id)} >
                  </Corpus>
                ))}
              </div>
            </CSSTransition>
          </SwitchTransition>
        </div>

        <BookLibraryModal booksLibrary={availableBooks} addBookCB={addBook} usedBooks={books} />
        <AudioPlayer play={isAudioPlaying} audioFile={audioPath} ended={e=>setAudioPlaying(false)}/>

      </div>
      </Animate>
    </div>
  );
}

export default App;
