import React, { Component } from "react";
import { isMobile } from "react-device-detect";
import { GiBurningBook } from "react-icons/gi";
import "./mainText.css";
import "./spinner.css";

const browserClass = isMobile ? "mobile" : "browser";

// adapts the size of the text to it's length
const getStyledText = text => {
  if (text.length > 300) return <p style={{ fontSize: "0.65em" }}>{text}</p>;
  if (text.length > 200) return <p style={{ fontSize: "0.7em" }}>{text}</p>;
  if (text.length > 150) return <p style={{ fontSize: "0.75em" }}>{text}</p>;
  if (text.length > 100) return <p style={{ fontSize: "0.85em" }}>{text}</p>;
  else return <p>{text}</p>;
};

export class MainText extends Component {
  render() {
    // replace empty text by illustrated instruction
    const text =
      this.props.text === "" ? (
        <p>
          <span>cliquer sur </span>
          <GiBurningBook />
          <span> pour génerer une citation</span>
        </p>
      ) : this.props.text === "loading" ? (
        <span>
          <p>calcul du modèle en cours...</p>
          <div className="lds-dual-ring"></div>
        </span>
      ) : (
        getStyledText(this.props.text)
      );
    return <div className={"main-text text-center vertical-center px-3 " + browserClass}>{text}</div>;
  }
}

export default MainText;
