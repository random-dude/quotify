import React from "react";

function VideoBackground(props) {
  return (
    <div className="video-container">
      <video autoPlay muted loop>
        <source src={props.filename} type="video/webm" />
      </video>
    </div>
  );
}

export default VideoBackground;
