import React, { Component } from "react";
import "./waitingScreen.css";

export class WaitingScreen extends Component {
  componentDidMount() {
    this.props.fetchSessionData(); // this CB will hide the loading page on API 200 response
  }
  render() {
    return (
      <div className="container-fluid waiting-screen">
        <div className="align-self-center content">
          <h1 className="text-center">initialisation de la bibliothèque...</h1>
          <div className="loader"></div>
        </div>
      </div>
    );
  }
}

export default WaitingScreen;
