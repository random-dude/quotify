import React from "react";
import { HiInformationCircle } from "react-icons/hi";

function About(props) {
  return (
    <div className="overlay container-fluid d-flex h-100" style={{ backgroundColor: "transparent" }}>
      <div className="row align-self-start justify-content-between p-2 ms-auto">
        <HiInformationCircle size={20} data-bs-toggle="modal" data-bs-target="#aboutModal" />
      </div>
      <div className="modal fade" tabIndex="-1" id="aboutModal">
        <div className="modal-dialog modal-dialog-centered">
          <div className="modal-content">
            <div className="modal-body">
              <h5 className="text-center">
                a propos de <b>quotify</b>
              </h5>
              <p className="text-center pt-2">
                merci d'avoir essayé <b>quotify</b> , un générateur de citation{" "}
                <a href="https://gitlab.com/random-dude/quotify">open-source</a>.
              </p>
              <p style={{ textAlign: "justify" }}>
                quotify utilise les <a href="https://fr.wikipedia.org/wiki/Cha%C3%AEne_de_Markov">chaines de Markov</a>{" "}
                pour créer des pseudo-citations à partir de textes existants. Ces textes sont segmentés en fragments
                signifiants par des techniques de{" "}
                <a href="https://fr.wikipedia.org/wiki/Traitement_automatique_des_langues">NLP</a>.
              </p>
              <p style={{ textAlign: "justify" }}>
                Un modèle calcule alors la probabilité pour chaque fragment d'être associé à un autre segment. Ils sont
                ensuite enchainés par ordre croissant de probabilité pour générer une phrase.
              </p>
              <p style={{ textAlign: "justify" }}>
                Les modèles utilisants plus d'un livre ont ainsi d'autant moins de chances de fournir une phrase
                cohérente que les livres adoptent un style différent (soit qu'un même fragment signifiant soit présent
                dans au moins deux livres).
              </p>
              <div className="row row-sm text-center pt-1">
                <div className="col" data-bs-toggle="modal" data-bs-target="#aboutModal" onClick={props.startTutorial}>
                  didacticiel
                </div>
                {/* <div className="col">
                  <a href="https://gitlab.com/random-dude/quotify" style={{ textDecoration: "none" }}>
                    code source
                  </a>
                </div> */}
                <div className="col" data-bs-toggle="modal" data-bs-target="#aboutModal">
                  fermer
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
}

export default About;
