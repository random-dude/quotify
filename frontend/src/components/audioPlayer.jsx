import React, { useEffect } from "react";

const audioFolder = process.env.REACT_APP_FRONTEND_AUDIO_PATH
  ? process.env.REACT_APP_FRONTEND_AUDIO_PATH
  : process.env.PUBLIC_URL;

function AudioPlayer(props) {
  useEffect(() => {
    const audioPath = audioFolder + "/" + props.audioFile;
    const player = new Audio(audioPath);
    player.onended = props.ended; // only allows player to play again when finished
    if (props.play) {
      player.play();
      console.log("playing", audioPath);
    }
    return () => {
      player.pause(); // stop the audio on unmount
    };
  });

  return <div></div>;
}

export default AudioPlayer;
