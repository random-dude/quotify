import React, { Component } from "react";
import { BrowserView, isMobile } from "react-device-detect";
import { FiBook } from "react-icons/fi";
import { RiCloseCircleFill } from "react-icons/ri";
import { CircularProgressbar, buildStyles } from "react-circular-progressbar";
import "react-circular-progressbar/dist/styles.css";
import "./corpus.css";

const browserClass = isMobile ? " mobile" : " desktop";

export class Corpus extends Component {
  state = {
    value: this.props.corpus.weight,
    lastUpdatedValue: this.props.corpus.weight,
  };
  touchLastValue = 0; // used to determine if user touches up or down
  mouseLastValue = [0, 0]; // used to determine if user moves the cursor up or down & left or right
  isClicked = false; // will be set to true if user click on the value bar

  // called on wheel event, add the deltaY*constant to current value
  handleWheel = e => {
    const delta = e.deltaY * 1.2;
    this.setConstrainedValue(delta);
  };

  // disable scroll on mouseEnter and touchMove, enable it on mouseLeave, touchEnd and component unmount
  disableScroll = e => {
    document.addEventListener("wheel", this.preventDefault, { passive: false });
    document.addEventListener("touchmove", this.preventDefault, { passive: false });
  };

  enableScroll = e => {
    document.removeEventListener("wheel", this.preventDefault, false);
    document.removeEventListener("touchmove", this.preventDefault, false);
  };

  preventDefault = e => {
    e = e || window.event;
    if (e.preventDefault) {
      e.preventDefault();
    }
    e.returnValue = false;
  };

  // only update weights if they changed when the cursor leaves or the touch ends
  handleLeave = e => {
    this.enableScroll();
    this.sendUpdatedValue();
  };

  // determines if the user moves up or down and by how much, then set the progressbar value
  handleTouchMove = e => {
    const delta = this.touchLastValue - e.touches[0].clientY;
    this.touchLastValue = e.touches[0].clientY;
    this.setConstrainedValue(delta);
  };

  // called on touchStart, set current touch value and disable scroll
  handleTouchStart = e => {
    this.disableScroll();
    this.touchLastValue = e.touches[0].clientY;
  };

  // will use either deltaX or deltaY depending on which changed the most
  handleMouseMove = e => {
    if (!this.isClicked || e.buttons != 1) return;
    const deltaX = this.mouseLastValue[0] - e.clientX;
    const deltaY = this.mouseLastValue[1] - e.clientY;
    const delta = Math.abs(deltaX) > Math.abs(deltaY) ? -deltaX : deltaY;
    this.mouseLastValue = [e.clientX, e.clientY];
    this.setConstrainedValue(delta);
  };

  // attached to this component
  handleMouseDown = e => {
    this.isClicked = true;
    this.mouseLastValue = [e.clientX, e.clientY];
  };

  // attached to the document
  handleMouseUp = e => {
    this.isClicked = false;
    this.sendUpdatedValue();
  };

  // add delta to the current value, make sure it stays constrained between 0~100
  setConstrainedValue = delta => {
    if (delta > 0) delta = Math.min(100, this.state.value + delta);
    else delta = Math.max(0, this.state.value + delta);
    this.setState({ ...this.state, value: delta });
  };

  // make the call to the API on mouseLeave or on touchEnd
  sendUpdatedValue = () => {
    if (this.state.value !== this.state.lastUpdatedValue) {
      this.props.adjustWeightCB(this.props.corpus.id, this.state.value);
      this.setState({ ...this.state, lastUpdatedValue: this.state.value });
    }
  };

  // theses document event listeners will make sure we keep tracking the cursor outside of this component
  componentDidMount() {
    document.addEventListener("mouseMove", this.handleMouseMove, { passive: false });
    document.addEventListener("mouseUp", this.handleMouseUp);
  }

  componentWillUnmount() {
    this.enableScroll();
    document.removeEventListener("mouseMove", this.handleMouseMove, false);
    document.removeEventListener("mouseUp", this.handleMouseUp, false);
  }

  render() {
    return (
      <div className={"col books" + browserClass}>
        <div className="book">
          <div className="book-border p-2">
            {/* book icon */}
            <BrowserView>
              <div className={"book-icon mx-auto px-2 "}>
                <FiBook size={40}></FiBook>
              </div>
            </BrowserView>

            {/* title and author */}
            <div className="book-body">
              <p
                className={"book-title" + browserClass}
                data-bs-toggle="modal"
                data-bs-target="#booksLibraryModal"
                onClick={this.props.bookToReplaceCB}>
                {this.props.corpus.title}
              </p>
              <p className={"book-author" + browserClass}>{this.props.corpus.author}</p>
            </div>

            {/* circular progressbar */}
            {this.props.multipleBooks && (
              <div
                className={"mx-auto adjust-weight circular-bar" + browserClass}
                onWheel={this.handleWheel}
                onMouseEnter={this.disableScroll}
                onMouseDown={this.handleMouseDown}
                onMouseMove={this.handleMouseMove}
                onMouseLeave={this.handleLeave}
                onTouchStart={this.handleTouchStart}
                onTouchEnd={this.handleLeave}
                onTouchMove={this.handleTouchMove}>
                <CircularProgressbar
                  value={this.state.value}
                  background
                  backgroundPadding={6}
                  styles={buildStyles({
                    backgroundColor: "rgba(17, 17, 17, .5)",
                    textColor: "#fff",
                    pathColor: "#fff",
                    trailColor: "rgba(255, 255, 255, .25)",
                  })}
                />
              </div>
            )}

            {/* close button */}
            <div className="mx-3 close-book">
              <RiCloseCircleFill size={30} onClick={e => this.props.removeCB(this.props.corpus.id)} />
            </div>
          </div>
        </div>
      </div>
    );
  }
}

export default Corpus;
