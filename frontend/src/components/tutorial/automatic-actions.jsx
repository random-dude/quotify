/*
actions are represented as a dict. They can be scheduled to run after a while or to wait for a condition
to become true.

Scheduled actions :
  {
    fn <str> : name of the function. The function can be defined in the class importing this file, on
              it's props or on the genericFunctions.js file
    params <any> : the parameters passed to the function. In case multiples parameters are passed, 
                  the function should define them as dict : function myFun({param1, param2})
    time <int> : the time after which the function should be called (in ms)
  }

"Wait for" actions : theses won't be scheduled but will periodically evaluate a condition until it passes or timeout occurs.
{
  fn <str> : set to keyword "wait for condition" (mandatory)
  params <dict> : the parameters of the waitFor() wrapper :
                   {
                     retryInterval <int, optional> : delay between two evaluations of the condition (in ms, defaults to 500)
                     timeout <int, optional> : time after which the condition will no longer be evaluated (in ms, defaults to 5000)
                   }
  condition <dict> : the function to call and it's parameters ;
                    {
                      fn <str> : the name of the validation function which should returns a bool
                      params <any> : this args will be passed to the validation function.
                    }
}
*/

export const actions = [
  // { fn: "setTitle", params: "bienvenue", time: 2000 },
  // { fn: "displayTitle", params: true, time: 0 },
  // { fn: "displayTitle", params: false, time: 2500 },
  {
    fn: "addArrow",
    params: { text: "ouvrir la bibliothèque", targetSelector: "#open-library", position: "left" },
    time: 1000,
  },
  { fn: "click", params: "#open-library", time: 1000 },
  {
    fn: "addArrow",
    params: { text: "sélectionner un livre", targetSelector: ".list-group-item .author", position: "right" },
    time: 1000,
  },
  { fn: "click", params: ".list-group-item", time: 2000 },
  { fn: "openRandomBook", params: null, time: 0 },
  { fn: "clearArrows", params: null, time: 0 },
  {
    fn: "addArrow",
    params: { text: "attendre que le modèle se charge", targetSelector: ".main-text p", position: "bottom" },
    time: 500,
  },
  {
    fn: "wait for condition",
    params: { retryInterval: 500, timeout: 5000 },
    condition: {
      fn: "elementContainsText",
      params: { selector: ".main-text p", text: "pour génerer une citation" },
    },
  },
  { fn: "clearArrows", params: [], time: 1500 },
  {
    fn: "addArrow",
    params: { text: "générer une citation", targetSelector: "#generate-quote", position: "top" },
    time: 500,
  },

  { fn: "generateSentence", params: [], time: 500 },
  { fn: "clearArrows", params: [], time: 3000 },
  {
    fn: "addArrow",
    params: { text: "activer/désactiver la lecture audio", targetSelector: "#mute", position: "top" },
    time: 500,
  },
  { fn: "clearArrows", params: [], time: 3000 },
  {
    fn: "addArrow",
    params: { text: "cliquer sur le titre pour changer le livre", targetSelector: ".book-title", position: "top" },
    time: 500,
  },
  { fn: "clearArrows", params: [], time: 3000 },
  {
    fn: "addArrow",
    params: { text: "ajoutons un second livre", targetSelector: "#open-library", position: "left" },
    time: 1000,
  },
  { fn: "click", params: "#open-library", time: 2000 },
  { fn: "openRandomBook", params: null, time: 500 },
  {
    fn: "wait for condition",
    params: { retryInterval: 500, timeout: 10000 },
    condition: {
      fn: "elementContainsText",
      params: { selector: ".main-text p", text: "pour génerer une citation" },
    },
  },
  { fn: "clearArrows", params: [], time: 0 },
  { fn: "click", params: ".list-group-item", time: 0 },
  {
    fn: "addArrow",
    params: { text: "réglons son influence", targetSelector: ".circular-bar", position: "top" },
    time: 500,
  },
  { fn: "mouseOver", params: ".circular-bar", time: 500 },
  { fn: "wheel", params: { selector: ".circular-bar", deltaY: -30 }, time: 500 },
  { fn: "clearArrows", params: [], time: 2000 },
  { fn: "closeRandomBook", params: null, time: 800 },
  { fn: "closeRandomBook", params: null, time: 1000 },
  {
    fn: "addArrow",
    params: { text: "à vous de jouer !", targetSelector: ".main-text p", position: "bottom" },
    time: 1000,
  },
  { fn: "clearArrows", params: [], time: 2500 },
  // { fn: "setTitle", params: "à vous de jouer", time: 500 },
  // { fn: "displayTitle", params: true, time: 100 },
];
