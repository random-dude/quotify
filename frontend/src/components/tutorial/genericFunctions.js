export const click = selector => {
  const element = getElement(selector);
  const clickEvent = new CustomEvent("click");
  if (element == null) console.log("unable to click on element ", selector);
  else element.dispatchEvent(clickEvent);
};

export const mouseOver = selector => {
  const element = getElement(selector);
  const event = new MouseEvent("mouseOver");
  if (element == null) console.log("unable to click on element ", selector);
  else element.dispatchEvent(event);
};

export const wheel = ({selector, deltaX, deltaY}) => {
  const element = getElement(selector);
  const event = document.createEvent('MouseEvents');
  event.initEvent('wheel',true,true);
  event.deltaX = (deltaX === undefined)? 0:deltaX;
  event.deltaY = (deltaY === undefined)? 0:deltaY;
  if (element == null) console.log("unable to click on element ", selector);
  else element.dispatchEvent(event);
}

export const getRandomElement = arr => {
  return arr[Math.floor(Math.random() * arr.length)];
};

export const getRandomString = () => {
  return Math.random().toString(36).substr(2, 5);
};

export const getElement = selector => {
  return document.querySelector(selector);
};

export const elementExists = selector => {
  return getElement(selector) != undefined;
};

export const elementDoesNotExist = selector => {
  return !elementExists(selector);
}

export const elementContainsText = ({selector, text, caseSensitive=false}) => {
  const element = getElement(selector);
  if (element != undefined) {
    if (caseSensitive) return element.textContent.includes(text);
    else return element.textContent.toLowerCase().includes(text.toLowerCase());
  } else {
    console.log("error searching text '", text, "' in element '", selector, "' : not found");
    return false;
  }
};

export const elementDoesNotContainText = ({selector, text, caseSensitive=false}) => {
  return ! elementContainsText({selector, text, caseSensitive});
}

export const elementChanged = ({selector, previousElement}) => {
  const element = getElement(selector);
  if (element == undefined) {
    console.log("element", selector, "did change since it disapeared")
    return true;
  }
  return element.outerHTML === previousElement.outerHTML;
}

export const elementHasClass = ({selector, className}) => {
  const element = getElement(selector);
  if (element == undefined) {
    console.log("unable to determine if element", selector, "has class", className, ": element not found");
    return false;
  }
  return element.classList.contains(className);
}

export const elementDoesNotHaveClass = ({selector, className}) => {return !elementHasClass({selector, className})}