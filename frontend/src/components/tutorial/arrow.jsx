import React, { Component } from "react";
import { FaLongArrowAltDown } from "react-icons/fa"; // the icon should be pointed down

export class Arrow extends Component {
  // this component creates an arrow always oriented toward a target. A descriptive text and/or a number
  // can be added. Its position relative to the target can be set as "top", "bottom", "left" or "right"
  state = {
    text: this.props.text, // leave to undefined to disable
    targetSelector: this.props.targetSelector, // css selector
    position: this.props.position === undefined ? "top" : this.props.position, // orientation relative to the target
    number: this.props.number, // number is drawn before the text, leave undefined to disable
    arrowSize: this.props.arrowSize === undefined ? 60 : this.props.arrowSize,
    padding: this.props.padding === undefined ? "1rem" : this.props.padding, // between the target and the arrow
    zIndex: this.props.zIndex === undefined ? "10000" : this.props.zIndex,
    className: this.props.className === undefined ? "arrow" : this.props.className, // class name of the arrow
    animateEvery: this.props.animateEvery, // will add a class named arrow-animated every xxx miliseconds
    animationDuration: this.props.animationDuration === undefined ? 1000 : this.props.animationDuration, // will remove said class after xxx milliseconds
    style: {}, // will be populated with the position and transformations needed
    target: undefined,
    isAnimationActive: false, // will toggle between " " and -animated
  };
  animateTimer = undefined;

  getCSSposition = () => {
    // calculate the position of this element relative to the target bounding box
    // and returns the CSS properties accordingly (top, left, transform and padding)
    const target = document.querySelector(this.state.targetSelector);
    if (target === undefined) {
      console.log("unable to place an arrow relative to", this.state.targetSelector);
      return {};
    } else {
      const targetPosition = target.getBoundingClientRect();
      this.setState({ ...this.state, target: target });
      if (this.state.position === "top")
        return {
          top: targetPosition.top - targetPosition.height + "px",
          left: targetPosition.left + targetPosition.width / 2 + "px",
          translate: "-50% -50%",
          paddingBottom: this.state.padding,
        };
      if (this.state.position === "bottom")
        return {
          top: targetPosition.top + targetPosition.height + "px",
          left: targetPosition.left + targetPosition.width / 2 + "px",
          translate: "-50% 0%",
          paddingTop: this.state.padding,
        };
      else if (this.state.position === "left")
        return {
          top: targetPosition.top - targetPosition.height / 2 + "px",
          left: targetPosition.left - targetPosition.width + "px",
          translate: "-50% 0%",
          paddingRight: this.state.padding,
        };
      else if (this.state.position === "right")
        return {
          top: targetPosition.top - targetPosition.height / 2 + "px",
          left: targetPosition.left + targetPosition.width + "px",
          translate: "0% 0%",
          paddingLeft: this.state.padding,
        };
    }
  };

  updateStyle = () => {
    // updates the CSS position and set the rotation of the arrow
    let rotation = {};
    switch (this.state.position) {
      case "bottom":
        rotation = { rotate: "180deg" };
        break;
      case "left":
        rotation = { rotate: "270deg" };
        break;
      case "right":
        rotation = { rotate: "90deg" };
        break;
      default:
        rotation = {};
    }
    this.setState({ ...this.state, style: this.getCSSposition(), rotation: rotation });
  };

  // sets the isAnimate flag and prepare it's deactivation via setTimeout
  animate = () => {
    const cb = () => setTimeout(this.unanimate, this.state.animationDuration);
    this.setState({ ...this.state, isAnimationActive: true }, cb);
  };

  unanimate = () => {
    this.setState({ ...this.state, isAnimationActive: false });
  };

  render() {
    return (
      <div
        // add the classname-animated if animation is currently active
        className={
          this.state.isAnimationActive
            ? this.state.className + " " + this.state.className + "-animated"
            : this.state.className
        }
        // apply CSS position, rotation and translates
        style={{ ...this.state.style, position: "absolute", zIndex: this.state.zIndex }}>
        {/* if the arrow is below the target, the arrow must be drawn above the other elements */}
        {this.state.position === "bottom" && (
          <FaLongArrowAltDown
            className={this.state.className + "-icon"}
            size={this.state.arrowSize}
            style={this.state.rotation}
          />
        )}
        {/* eventually add the text and/or the number */}
        {this.state.number !== undefined && <h4 className={this.state.className + "-number"}>{this.state.number}</h4>}
        {this.state.text !== undefined && <p className={this.state.className + "-text"}>{this.state.text}</p>}
        {/* if the arrow is above the target or on it's side it should be drawn underneath the other elements */}
        {this.state.position !== "bottom" && (
          <FaLongArrowAltDown
            className={this.state.className + "-icon"}
            size={this.state.arrowSize}
            style={this.state.rotation}
          />
        )}
      </div>
    );
  }

  // for the arrow to stay on target, it's position should be re-calculated whenever the window is scrolled or resized
  componentDidMount() {
    this.updateStyle();
    window.addEventListener("scroll", this.updateStyle, { passive: false });
    window.addEventListener("resize", this.updateStyle, { passive: false });
    window.addEventListener("load", this.updateStyle, { passive: false });
    if (this.state.animateEvery !== undefined && this.state.animateEvery > 0) {
      this.animateTimer = setInterval(this.animate, this.state.animateEvery);
    }
  }

  componentWillUnmount() {
    window.removeEventListener("scroll", this.updateStyle, false);
    window.removeEventListener("resize", this.updateStyle, false);
    window.removeEventListener("load", this.updateStyle, false);
    if (this.animateTimer !== undefined) clearInterval(this.animateTimer);
  }
}

export default Arrow;
