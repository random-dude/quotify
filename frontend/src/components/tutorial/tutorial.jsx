import React, { Component } from "react";
import { CSSTransition, SwitchTransition } from "react-transition-group";
import { IoPlaySkipBack, IoPlaySkipForward, IoRefreshCircle, IoCloseCircle } from "react-icons/io5";
import { Arrow } from "./arrow";
// import { actions } from "./automatic-actions";
import { actions } from "./interactive-actions";
import * as functions from "./genericFunctions";
import "./tutorial.css";
import "../../animations.css";

export class Tutorial extends Component {
  constructor(props) {
    super();
    this.state = {
      showWelcome: true,
      isOverlayBlocking: true, // set a very high z-index to the overlay to prevent user interacting with the app
      arrows: [], // will contain arrows dict, rendered to <Arrow/>
    };
    this.currentStep = 0;
    this.waitForTimer = undefined; // setInterval will be attached here
    this.waitForExpires = 0; // timeout corresponding to Date.now()+timeout
  }

  // ------------------------------------- hooks -------------------------------------

  showWelcome = display => this.setState({ ...this.state, showWelcome: display });
  setOverlayBlocking = display => this.setState({ ...this.state, isOverlayBlocking: display });
  addArrow = arrow => {
    Object.assign(arrow, { animateEvery: 5000, animationDuration: 1000 });
    this.setState({ ...this.state, arrows: [...this.state.arrows, <Arrow {...arrow} />] });
  };
  clearArrows = () => this.setState({ ...this.state, arrows: [] });

  // ----------------------- methods specific to this tutorial ----------------------

  openRandomBook = () => {
    this.props.addBookCB(functions.getRandomElement(this.props.booksLibrary).id);
  };
  closeRandomBook = () => {
    this.props.removeBookCB(functions.getRandomElement(this.props.usedBooks).id);
  };

  // ------------------- generic methods used by the demo/tutorial -------------------

  wrap = fn => () => {
    // wrap the function to be called later by adding the next step callback to it
    console.log("running task #", this.currentStep);
    fn();
    this.runTutorial(this.currentStep++);
  };

  waitFor = (condition, { retryInterval = 500, timeout = 5000 }) => {
    // sets an interval to schedule a periodic test until a given condition is fullfilled or timeout is reached.
    // Replaces the wrap function.
    this.waitForExpires = Date.now() + timeout;
    const checkCondition = conditionFn => {
      if (Date.now() > this.waitForExpires) {
        console.log("waitFor expired");
        clearInterval(this.waitForTimer);
        this.onTimeout();
      } else if (conditionFn() === true) {
        clearInterval(this.waitForTimer);
        this.runTutorial(this.currentStep++);
      }
    };
    this.waitForTimer = window.setInterval(() => checkCondition(condition), retryInterval);
  };

  searchFunction = (fnName, args) => {
    // returns a function callback from the functions name and its arguments
    // looks for the function's name in this class
    if (Object.keys(this).includes(fnName)) return () => this[fnName](args);
    // then in the props (for callbacks)
    else if (Object.keys(this.props).includes(fnName)) return () => this.props[fnName](args);
    // then into the genericFunctions.js file
    else if (Object.keys(functions).includes(fnName)) return () => functions[fnName](args);
    // if not found anywhere :
    else return undefined;
  };

  runTutorial = step => {
    // will go through an action and schedule it's execution

    // last action reached
    if (step === actions.length) {
      this.currentStep = 0;
      this.afterTutorial();
    } else {
      const action = actions[step];
      // special action "wait for condition" uses a specific wrapper
      if (action.fn === "wait for condition") {
        const condition = this.searchFunction(action.condition.fn, action.condition.params);
        if (condition === undefined) console.log("error executing wait for condition", action.condition.fn);
        else {
          this.waitFor(condition, action.params);
        }
        return;
      }

      // schedule an action if time is set or call it now
      let functionCall = this.searchFunction(action.fn, action.params);
      if (functionCall !== undefined) {
        console.log("scheduling task #", step, "to be called in ", action.time, "ms :", action.fn, action.params);
        if (action.time > 0) setTimeout(this.wrap(functionCall), action.time);
        else this.wrap(functionCall)();
      } else console.log(action.fn, "not found, skipping step", step);
    }
  };

  startTutorial = () => {
    this.setState({ ...this.state, showWelcome: false, arrows: [] }, () => this.setOverlayBlocking(false));
    // reset (in case the tutorial is restarted)
    if (this.waitForTimer !== undefined) clearInterval(this.waitForTimer);
    this.currentStep = 0;
    // empty model
    this.props.usedBooks.forEach(b => {
      this.props.removeBookCB(b.id);
    });
    // starts the demo
    console.log("starting tutorial");
    this.runTutorial(this.currentStep++);
  };

  afterTutorial = () => {
    console.log("finished tutorial");
    // empty model
    this.props.usedBooks.forEach(b => {
      this.props.removeBookCB(b.id);
    });
    // delete this component
    setTimeout(this.props.closeTutorial, 1000);
  };

  closeTutorial = () => {
    this.showWelcome(false);
    // wait for the animation to end
    setTimeout(this.props.closeTutorial, 200);
  };

  onTimeout = () => {
    // called when a waitFor expires
    this.closeTutorial();
    // continue anyway ?
    // this.runTutorial(this.currentStep++);
  };

  componentDidMount() {
    this.showWelcome(!this.props.startOnLoad);
    if (this.props.startOnLoad === true) this.startTutorial();
  }

  componentWillUnmount() {
    if (this.waitForTimer !== undefined) clearInterval(this.waitForTimer);
  }

  render() {
    return (
      <div className={"overlay container-fluid d-flex h-100" + (this.state.isOverlayBlocking ? " blocking" : "")}>
        {/* welcome modal */}
        <SwitchTransition>
          <CSSTransition classNames="fade" timeout={200} key={this.state.showWelcome}>
            {(this.state.showWelcome && (
              <div className="row align-self-center mx-auto" id="welcome">
                <h1 className="pt-3">bienvenue</h1>
                <p className="pt-3" style={{ fontSize: "x-large" }}>
                  <b>quotify</b> est un générateur de citation
                </p>
                <p style={{ fontSize: "larger" }}>
                  qui utilise l'intelligence artificielle pour produire des pseudo-citations à partir de livres
                  existants
                </p>
                <div className="col">
                  <p style={{ fontSize: "x-large" }} onClick={this.startTutorial} className="clickable">
                    visite guidée
                  </p>
                </div>
                <div className="col">
                  <p style={{ fontSize: "x-large" }} onClick={this.closeTutorial} className="clickable">
                    passer
                  </p>
                </div>
              </div>
            )) || (
              // control bar to navigate inside the tutorial
              <div className="row align-self-start justify-content-between pt-1" id="tutorial-controls">
                <div className="row">
                  <p>didacticiel</p>
                </div>
                {/* <div className="row">
                  <div className="col">
                    <IoPlaySkipBack size={30} />
                  </div>
                  <div className="col">
                    <IoPlaySkipForward size={30} />
                  </div>
                </div> */}
                <div className="row">
                  <div className="col">
                    <IoRefreshCircle size={30} onClick={this.startTutorial} />
                  </div>
                  <div className="col">
                    <IoCloseCircle size={30} onClick={this.props.closeTutorial} />
                  </div>
                </div>
              </div>
            )}
          </CSSTransition>
        </SwitchTransition>

        {/* arrows */}
        <CSSTransition classNames="fade" timeout={200} key={this.state.arrows.length}>
          <div>{this.state.arrows}</div>
        </CSSTransition>
      </div>
    );
  }
}
export default Tutorial;
