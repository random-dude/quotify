/*
actions are represented as a dict. They can be scheduled to run after a while or to wait for a condition
to become true.

Scheduled actions :
  {
    fn <str> : name of the function. The function can be defined in the class importing this file, on
              it's props or on the genericFunctions.js file
    params <any> : the parameters passed to the function. In case multiples parameters are passed, 
                  the function should define them as dict : function myFun({param1, param2})
    time <int> : the time after which the function should be called (in ms)
  }

"Wait for" actions : theses won't be scheduled but will periodically evaluate a condition until it passes or timeout occurs.
{
  fn <str> : set to keyword "wait for condition" (mandatory)
  params <dict> : the parameters of the waitFor() wrapper :
                   {
                     retryInterval <int, optional> : delay between two evaluations of the condition (in ms, defaults to 500)
                     timeout <int, optional> : time after which the condition will no longer be evaluated (in ms, defaults to 5000)
                   }
  condition <dict> : the function to call and it's parameters ;
                    {
                      fn <str> : the name of the validation function which should returns a bool
                      params <any> : this args will be passed to the validation function.
                    }
}
*/

const userTimeout = 120000; // actions will timeout after waiting 2 minutes for user input
const addArrowDelay = 200; // arrows will take 200ms to appear
const loadBook = arrowText => {
  return [
    {
      fn: "wait for condition",
      params: { retryInterval: 500, timeout: userTimeout },
      condition: {
        fn: "elementHasClass",
        params: { selector: "#booksLibraryModal", className: "show" },
      },
    },
    { fn: "clearArrows", params: null, time: addArrowDelay },
    {
      fn: "addArrow",
      params: { text: arrowText, targetSelector: ".list-group-item .author", position: "right" },
      time: addArrowDelay,
    },
    {
      fn: "wait for condition",
      params: { retryInterval: 500, timeout: userTimeout },
      condition: {
        fn: "elementDoesNotHaveClass",
        params: { selector: "#booksLibraryModal", className: "show" },
      },
    },
    { fn: "clearArrows", params: null, time: 0 },
    {
      fn: "wait for condition",
      params: { retryInterval: 500, timeout: userTimeout },
      condition: {
        fn: "elementContainsText",
        params: { selector: ".main-text p", text: "pour génerer une citation" },
      },
    },
  ];
};

export const actions = [
  {
    fn: "addArrow",
    params: { text: "ouvrir la bibliothèque", targetSelector: "#open-library", position: "left" },
    time: 500,
  },
  ...loadBook("sélectionner un livre"),
  {
    fn: "addArrow",
    params: { text: "générer une citation", targetSelector: "#generate-quote", position: "top" },
    time: addArrowDelay,
  },
  {
    fn: "wait for condition",
    params: { retryInterval: 500, timeout: userTimeout },
    condition: {
      fn: "elementDoesNotContainText",
      params: { selector: ".main-text p", text: "pour génerer une citation" },
    },
  },
  { fn: "clearArrows", params: [], time: 0 },
  {
    fn: "addArrow",
    params: { text: "activer/désactiver la lecture audio", targetSelector: "#mute", position: "top" },
    time: addArrowDelay,
  },
  { fn: "clearArrows", params: [], time: 3000 },
  {
    fn: "addArrow",
    params: { text: "chaque clic génère une nouvelle citation", targetSelector: "#generate-quote", position: "top" },
    time: 500,
  },
  { fn: "clearArrows", params: [], time: 5000 },
  {
    fn: "addArrow",
    params: { text: "cliquer sur le titre pour changer le livre", targetSelector: ".book-title", position: "top" },
    time: 500,
  },
  ...loadBook("sélectionner un autre livre"),
  {
    fn: "addArrow",
    params: {
      text: "plusieurs livres peuvent être utilisés en même temps",
      targetSelector: ".main-text",
      position: "bottom",
    },
    time: addArrowDelay,
  },
  { fn: "clearArrows", params: [], time: 4000 },
  {
    fn: "addArrow",
    params: { text: "ajoutons un second livre", targetSelector: "#open-library", position: "left" },
    time: addArrowDelay,
  },
  ...loadBook("sélectionner un autre livre"),
  {
    fn: "addArrow",
    params: { text: "la citation utilisera les deux livres", targetSelector: "#generate-quote", position: "top" },
    time: addArrowDelay,
  },
  {
    fn: "wait for condition",
    params: { retryInterval: 500, timeout: userTimeout },
    condition: {
      fn: "elementDoesNotContainText",
      params: { selector: ".main-text p", text: "pour génerer une citation" },
    },
  },
  { fn: "clearArrows", params: [], time: 0 },
  {
    fn: "addArrow",
    params: { text: "l'influence de chacun est réglable ici", targetSelector: ".circular-bar", position: "top" },
    time: 500,
  },
  { fn: "mouseOver", params: ".circular-bar", time: 1000 },
  { fn: "wheel", params: { selector: ".circular-bar", deltaY: -30 }, time: 500 },
  { fn: "clearArrows", params: [], time: 3000 },
  {
    fn: "addArrow",
    params: { text: "à vous de jouer !", targetSelector: ".main-text p", position: "bottom" },
    time: 2000,
  },
  { fn: "clearArrows", params: [], time: 2500 },
];
