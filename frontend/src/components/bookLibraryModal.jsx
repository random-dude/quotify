import React from "react";
// TODO : include uncurated books grayed out ?

function BookLibraryModal(props) {
  // generate entries for each book in the book array
  const booksList = props.booksLibrary.map(book => {
    const isBookInUse = props.usedBooks.map(b => b.id).includes(book.id);
    return (
      <li
        className={isBookInUse ? "list-group-item disabled" : "list-group-item"}
        key={Math.random()}
        data-bs-toggle="modal"
        data-bs-target="#booksLibraryModal"
        onClick={e => props.addBookCB(book.id)}>
        <span className="title">{book.title} </span>
        <span className="author">{book.author}</span>
      </li>
    );
  });

  return (
    <div className="modal fade" tabIndex="-1" id="booksLibraryModal">
      <div className="modal-dialog modal-dialog-centered">
        <div className="modal-content">
          <div className="modal-header">
            <h5 className="modal-title text-center">Librairie</h5>
          </div>
          <div className="modal-body">
            <ul className="list-group">{booksList}</ul>
          </div>
        </div>
      </div>
    </div>
  );
}
export default BookLibraryModal;
