import React from "react";
import { isMobile } from "react-device-detect";
import { GiBurningBook, GiOpenBook, GiShouting, GiSilenced } from "react-icons/gi";

const iconSize = isMobile ? 50 : 80;

function ControlButtons(props) {
  return (
    <div className="row py-4 control-buttons row-cols-auto justify-content-center">
      <div
        className="col px-5"
        data-bs-toggle="modal"
        data-bs-target="#booksLibraryModal"
        onClick={() => props.setBookToReplace(null)}>
        <GiOpenBook size={iconSize} id="open-library" />
      </div>
      <div className="col px-5 clickable" onClick={props.toggleMuted}>
        {(props.isMuted && <GiSilenced size={iconSize} id="mute" />) || <GiShouting size={iconSize} id="mute" />}
      </div>
      <div
        className="col px-5 clickable"
        data-bs-toggle="tooltip"
        title="écouter une prophétie"
        onClick={props.buttonCB}>
        <GiBurningBook size={iconSize} id="generate-quote" />
      </div>
    </div>
  );
}

export default ControlButtons;
