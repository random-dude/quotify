from datetime import datetime, timedelta
import os
import json
import config
import markov

MODEL_LIFESPAN = {"days":7} # uses timedelta format
USER_MODEL_DATABASE = ""

class UserModel():
    """ will contain a model per user, it's configuration (muted, show tutorial) and a timestamp
    """
    def __init__(self, uid:str):
        self.uid = uid
        self.model = markov.Model()
        self.isMuted = False
        self.showTutorial = True
        self.creationDate = datetime.now()

    def saveToDict(self) -> dict:
        return {"uid": self.uid, "books": self.model.getActiveBooks(), "isMuted": self.isMuted,
                "showTutorial": self.showTutorial, "creationDate": self.creationDate.isoformat()}

    def loadFromDict(self, modelAsDict):
        print(f'loading model for user {modelAsDict["uid"]} :')
        books = modelAsDict["books"]
        self.isMuted = modelAsDict["isMuted"]
        self.showTutorial = modelAsDict["showTutorial"]
        self.creationDate = datetime.fromisoformat(modelAsDict["creationDate"])
        for book in books :
            print(f'  adding book {book["title"]} with weight {book["weight"]}')
            self.model.addBook(book)
            self.model.updateWeight(book["id"], book["weight"]/100.)

    def hasExpired(self) -> bool :
        return datetime.now() > self.creationDate + timedelta(**MODEL_LIFESPAN)
      
def saveUserModelsToJson(userModels:list, path=config.Markov.userModelDatabase):
    """write every UserModel object contained inside the userModels list into a file

    Args:
        userModels (list): list of UserModel objects
        path (str, optional): path to the json file. Defaults to config.Markov.userModelDatabase.
    """
    print("saving", len(userModels), "models to ", path)
    with open(path, "w+") as f:
        json.dump({uid:model.saveToDict() for uid, model in userModels.items() if not model.hasExpired()}, f)

def loadUserModelsFromJson(path=config.Markov.userModelDatabase) -> dict:
    """returns a dict representing the UserModels contained in a json file

    Args:
        path (str, optional): path to the json file. Defaults to config.Markov.userModelDatabase.

    Returns:
        dict: {uid : UserModel.saveToDict()}
    """
    if not os.path.isfile(path) : return {}
    with open(path) as f:
        modelsAsDict = json.load(f)
        print("loaded", len(modelsAsDict), "models from", path)
        return modelsAsDict