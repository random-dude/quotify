import pytest
import os
import glob
import urllib.parse
import json
import random
import config
from fastapi.testclient import TestClient

TEST_BOOK_FILENAME = "test-corpus.json"

client = None
availablesBooks = None
userID = "newUser"
usedBooks = None
audioFileName = None

# chdir to the folder of this file
os.chdir(os.path.abspath(os.path.dirname(__file__)))

@pytest.fixture(autouse=True)
def realBook():
    """setup the config in production mode and verify that at least two books are present. 
    Since this file will test hybrid model quote generation, all the tests will fail without a book
    """
    global client
    from app import app
    client = TestClient(app)
    txtFiles = glob.glob(config.Markov.corpusPath + "/*.txt")
    if len(txtFiles) < 2:
        pytest.fail(f"You need at least two books in {config.Markov.corpusPath} to test the API")
    return


def test_get_session_data():
    """set the global var availablesBooks to the content of the books folder and test it's composition"""
    global availablesBooks, userID
    response = client.get(f"/api/{userID}/getSessionData")
    assert response.status_code == 200
    data = response.json()
    assert type(data) == dict
    print(data)
    assert all( k in data.keys() for k in ("availablesBooks", "uid", "modelNeedsReload"))
    userID = data["uid"]
    availablesBooks = data["availablesBooks"]
    assert data["modelNeedsReload"] is True
    assert type(availablesBooks) == list
    assert len(availablesBooks) > 0
    assert all(k in availablesBooks[0].keys() for k in ("title", "author", "filename", "isCurated"))

def test_get_books_in_use(expectedOutput=[], ignoreWeights=False): 
    """get the books used by the model and test them against a predicted output

    Args:
        expectedOutput (list, optional): state of the books used by the model.
                                         Defaults to no books used : [].
        ignoreWeights (bool, optional, defaults to False) : ignore weight when comparing the output to an expected result
                                                            to avoid failing the test due to unpredictable weight balancing when more than one book is added.
    """
    global usedBooks
    response = client.get(f"/api/{userID}/getCurrentlyUsedBooks")
    assert response.status_code == 200
    usedBooks = response.json()
    if ignoreWeights :
        for book in usedBooks + expectedOutput : 
            if "weight" in book.keys() : del(book["weight"])
    print("expected result :", expectedOutput)
    print("actual output :", usedBooks)
    print("available books", availablesBooks)
    assert usedBooks == expectedOutput

def test_add_real_book():
    """will test the composition of model.books after adding a random book"""
    realBook = random.choice(availablesBooks)
    response = client.get(f"/api/{userID}/addBookById/"+urllib.parse.quote(realBook["id"]))
    assert response.status_code == 200
    expectedOutput = [dict(realBook, **{"weight":100})]
    test_get_books_in_use(expectedOutput)
    assert len(usedBooks) > 0

def test_generate_sentence():
    """test the quote is returned and well formatted"""
    response = client.get(f"/api/{userID}/generateSentence")
    assert response.status_code == 200
    sentence = json.loads(response.content)
    assert type(sentence) == str
    assert len(sentence) > 10
    assert sentence.startswith("«")
    assert sentence.endswith("»")

def test_replace_book():
    """replace the current book by a random one, test the state of model.books"""
    randomBook = random.choice(availablesBooks)
    response = client.post(f"/api/{userID}/replaceBookById", json={
        "toReplace" : usedBooks[0]["id"],
        "newBookId" : randomBook["id"]
    })
    assert response.status_code == 200
    test_get_books_in_use([dict(randomBook, **{"weight":100})])

def test_add_another_book():
    """add another random book, and test the new state of model.books"""
    realBook = random.choice(availablesBooks)
    response = client.get(f"/api/{userID}/addBookById/"+urllib.parse.quote(realBook["id"]))
    assert response.status_code == 200
    expectedOutput = usedBooks + [realBook]
    test_get_books_in_use(expectedOutput, ignoreWeights=True)

def test_update_weight():
    """test the API side of this function, can't be validated due to the balanceWeight method"""
    response = client.post(f"/api/{userID}/updateBookWeight", json={
        "id" : usedBooks[0]["id"],
        "value" : 50})
    assert response.status_code == 200

def test_generate_sentence_from_two_books():
    """failing now but not earlier indicates specific hybrid-model related issues"""
    test_generate_sentence()

def test_get_audio():
    """ will test the API side of the audio generation only"""
    global audioFileName
    response = client.get(f"/api/{userID}/getAudio")
    assert response.status_code == 200
    audioFileName = json.loads(response.content)
    assert type(audioFileName) == str
    if config.Markov.TTSchoice == ("picoTTS"):
        assert audioFileName.endswith(".wav")
    elif config.Markov.TTSchoice == ("gTTS"):
        assert audioFileName.endswith(".mp3")

def test_audio_folder_exists():
    """may seems redondant with test_config but allow to weed out path problems in this prod configuration"""
    assert os.path.isdir(config.Markov.audioPath)

def test_audio_file_exists():
    """test picotts, paths and API alltogether"""
    path = os.path.join(config.Markov.audioPath, audioFileName)
    assert os.path.isfile(path)
    os.remove(path)

def test_remove_book():
    """test the state of model.books after removing one book"""
    response = client.delete(f"/api/{userID}/removeBookById/"+urllib.parse.quote(usedBooks[0]["id"]))
    assert response.status_code == 200
    expectedOutput = usedBooks[1:]
    test_get_books_in_use(expectedOutput, ignoreWeights=True)
    assert len(usedBooks) == 1

