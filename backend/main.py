import sys
import uvicorn
import config

reload = len(sys.argv) > 1 and sys.argv[1].lower() in ("-d", "--debug", "debug")

if __name__ == "__main__":
    uvicorn.run("app:app", host=config.API.host,
                port=config.API.port, reload=reload)
