import os
from shutil import copy
import json
import pytest
import books
import config

DUMMY_TEXT = """
Lorem ipsum dolor sit amet, consectetur adipiscing elit. Fusce maximus bibendum magna vitae tincidunt. Sed scelerisque dui sem, sit amet tempor tortor semper vitae. Phasellus vehicula turpis vitae massa blandit, vel ultrices libero vehicula. Maecenas leo metus, sollicitudin et vestibulum congue, egestas vel massa. Sed eu consequat lorem, nec dictum nulla. Aliquam hendrerit dictum cursus. Curabitur finibus nisl quis imperdiet blandit. Quisque facilisis et mauris vitae iaculis. Suspendisse vestibulum facilisis efficitur.
Aliquam ullamcorper dapibus ipsum, vitae molestie ipsum commodo sed. In tincidunt bibendum tempor. Etiam posuere a ex imperdiet fermentum. Quisque et feugiat justo. Curabitur eu justo nec ipsum semper molestie eget eget nulla. Nullam tortor purus, convallis sit amet mi vel, commodo condimentum ante. Phasellus ac dignissim nunc. Mauris dignissim sem a ex aliquet, quis gravida risus tempus. Etiam vel enim eget quam iaculis facilisis in sed lorem.
Cras sit amet magna massa. Aliquam ac porta augue. Interdum et malesuada fames ac ante ipsum primis in faucibus. Pellentesque vestibulum ac felis sed dapibus. Aenean a erat congue, viverra dui eget, pharetra lorem. Nulla facilisi. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia curae;
Donec rutrum aliquam porta. Donec sollicitudin fermentum tincidunt. Nulla facilisi. Nam aliquam tellus ligula, eu sagittis ex efficitur non. Aliquam diam ligula, semper non orci eget, gravida lacinia libero. Vivamus tempus ut orci eu pulvinar. Aenean vestibulum bibendum risus, ac hendrerit quam lobortis sit amet. Proin eu sodales mauris. Donec vestibulum nunc posuere magna posuere dapibus.
Maecenas sodales feugiat augue in pretium. Sed quis urna vulputate, feugiat turpis blandit, efficitur dui. Nam hendrerit arcu placerat scelerisque consectetur. Nullam euismod, metus in egestas condimentum, lorem tortor malesuada orci, laoreet scelerisque orci felis at turpis. Nam eleifend eros non mi feugiat fermentum. Praesent elit elit, viverra nec ligula et, suscipit ornare ex. Praesent ullamcorper vehicula justo et viverra.
Suspendisse potenti. In urna mi, elementum aliquam varius eget, dictum in nisi. Curabitur id mattis magna. Nulla tincidunt ipsum vel molestie suscipit. Morbi vestibulum tortor at dui pharetra, eget fermentum dui vestibulum. Vivamus feugiat, risus et aliquet dictum, nibh velit vestibulum nibh, ut rhoncus magna tellus vitae felis. Sed eu aliquam nisl, et vehicula odio. Aliquam erat volutpat. Proin ipsum erat, auctor quis est et, sollicitudin accumsan odio. Donec pulvinar rutrum nisl, a efficitur justo semper ac.
Donec pulvinar mi in nunc ultricies molestie. Aenean scelerisque convallis diam sit amet viverra. Duis diam erat, rutrum quis tellus quis, maximus consectetur orci. Proin pellentesque, odio vel ullamcorper placerat, est ipsum maximus nulla, non maximus dui erat vel elit. Fusce fermentum molestie neque in dictum. Pellentesque bibendum efficitur congue. Curabitur elementum sem est, a bibendum odio imperdiet eu. Morbi elit nisi, blandit ut nisi a, suscipit lobortis metus. Mauris euismod vel nisl at varius. Suspendisse at massa felis. Ut ac ante efficitur, ultricies leo et, vulputate nunc. In nec magna augue. Donec tristique erat a pharetra imperdiet. Nam a neque metus. Donec ligula libero, vulputate et mi dignissim, fermentum scelerisque odio. Morbi semper, lectus at bibendum blandit, mauris tellus ultrices enim, vel tristique nulla arcu ultricies magna.
Fusce semper tortor quis diam commodo, eget facilisis elit lacinia. Ut arcu risus, consectetur ac nisl vitae, tincidunt sagittis sapien. Ut venenatis ultricies lorem, sed pharetra ante hendrerit non. Sed eget lacus nec sapien bibendum molestie. Duis eu vestibulum felis. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec eros lorem, aliquam a bibendum ac, fermentum eu est. Nam vel hendrerit magna. Praesent scelerisque neque sed ante commodo, quis elementum augue rhoncus. Ut lobortis eleifend dui in dictum. Donec a aliquet ipsum, vel facilisis ex. Nullam et ullamcorper mauris, sit amet ultricies libero. Praesent libero felis, sodales eu sodales dignissim, consequat id eros. Suspendisse volutpat interdum neque, in condimentum enim pretium ut. Ut et condimentum metus. Duis vulputate mollis augue vitae commodo.
Proin pretium quam elementum enim venenatis, sit amet aliquam sem mattis. Cras sit amet ligula sagittis, efficitur justo vel, vulputate mauris. Interdum et malesuada fames ac ante ipsum primis in faucibus. Pellentesque turpis velit, ultrices a leo at, convallis vulputate enim. Maecenas et risus sem. In hac habitasse platea dictumst. Aliquam hendrerit augue eu lacinia mollis. Suspendisse potenti. Nam molestie ex ut tempus interdum. Pellentesque semper euismod scelerisque. Vivamus dignissim ligula magna, eu dictum turpis facilisis tincidunt. Integer quis arcu hendrerit, dignissim nulla vitae, congue risus. Suspendisse dignissim vehicula urna, at rutrum nunc dapibus et.
Etiam id tortor leo. Maecenas rutrum magna a fringilla dapibus. Quisque at orci vitae tortor feugiat fermentum. Pellentesque pretium tortor ut odio vestibulum luctus. Cras mollis nunc vel elit suscipit imperdiet. Nunc non leo id magna dictum aliquet. Donec maximus eleifend nunc, ac bibendum ipsum suscipit nec. Vivamus id venenatis enim. In finibus lacinia neque, sit amet laoreet justo mattis a.
"""
DUMMY_FILENAME = "test_loremIpsum.txt"
DUMMY_BOOK = {"title":"dummy title", "author":"dummy author", "isCurated":True, "filename":DUMMY_FILENAME, "id":"ja2ej2pvfgxnfuunvst7"}
TEST_BOOK_FILENAME = "test-corpus.json"

jsonModelPaths = []

# chdir to the folder of this file
os.chdir(os.path.abspath(os.path.dirname(__file__)))

@pytest.fixture()
def writeDummyBook():
    """creates a dummy text files which is faster to precompute than a true book   """
    dummyBookPath = os.path.join(config.Markov.corpusPath, DUMMY_FILENAME)
    with open(dummyBookPath, "wt") as f:
        f.write(DUMMY_TEXT)
    yield
    os.remove(dummyBookPath)
    for path in jsonModelPaths :
        if os.path.isfile(path) : os.remove(path)

def test_precompute(writeDummyBook):
    global jsonModelPaths
    for stateSize in (config.Markov.singleStateSize, config.Markov.hybridStateSize):
        books.preComputeModel(DUMMY_FILENAME, stateSize)
        filename = os.path.join(str(stateSize), DUMMY_FILENAME)
        jsonModelPaths.append(books.getJsonPath(books.prefixPath(filename)))
        with open(jsonModelPaths[-1], "r") as f :
            json.load(f)

def test_book_class_on_real_book():
    global jsonModelPaths
    """ uses a real bool precomputed to json to generate a sentence"""
    testBook = DUMMY_BOOK
    for stateSize in (config.Markov.singleStateSize, config.Markov.hybridStateSize):
        copy(TEST_BOOK_FILENAME, os.path.join(config.Markov.corpusPath,str(stateSize)))
        jsonModelPaths.append(os.path.join(config.Markov.corpusPath,str(stateSize), TEST_BOOK_FILENAME))
    testBook["filename"] = TEST_BOOK_FILENAME
    dummyBook = books.Book(DUMMY_BOOK)
    assert dummyBook.title == DUMMY_BOOK["title"]
    assert dummyBook.author == DUMMY_BOOK["author"]
    assert dummyBook.filename == DUMMY_BOOK["filename"]
    assert dummyBook.isCurated == DUMMY_BOOK["isCurated"]
    assert dummyBook.id == DUMMY_BOOK["id"]
    assert dummyBook.isSingle == True
    assert type(dummyBook.generateSentence(maxTries=200)) == str
