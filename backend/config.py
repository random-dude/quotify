import os
SCRIPT_PATH = os.path.abspath(os.path.dirname(__file__))
PARENT_PATH = os.path.abspath(os.path.join(SCRIPT_PATH, os.pardir))

class API:
    # defaults will be used in the tests
    host = os.getenv("API_HOST","localhost")
    port = int(os.getenv("API_PORT", "8000"))
    HTTPhost = os.getenv("HTTP_HOST","localhost")
    HTTPport = int(os.getenv("HTTP_PORT", "3000"))
    origins = [f"https://{HTTPhost}:{HTTPport}",
                f"https://{HTTPhost}",
                f"http://{HTTPhost}:{HTTPport}",
                f"http://{HTTPhost}",
                f"{HTTPhost}:{HTTPport}",
                f"{HTTPhost}",
                ]
    reload = bool(os.getenv("RELOAD_BACKEND", "True"))

class Markov:
    # audioPath = f"../frontend/public/" # webpack will detect changes on this folder and reload the page
    audioPath = os.getenv("API_AUDIO_PATH", os.path.join( PARENT_PATH, "frontend/build/"))
    corpusPath = os.path.join(SCRIPT_PATH, "corpus")
    booksSummary = "booksSummary.json"
    picoTTSpath = os.path.join(SCRIPT_PATH, "picotts/dist/bin/pico2wave")
    TTSchoice = os.getenv("USE_TTS", "picoTTS")
    singleStateSize = int(os.getenv("STATE_SIZE_SINGLE", "3"))
    hybridStateSize = int(os.getenv("STATE_SIZE_HYBRID", "2"))
    for stateSize in (singleStateSize, hybridStateSize):
        thisStatePath = os.path.join(corpusPath, str(stateSize))
        if not os.path.isdir(thisStatePath):
            print(f"folder for state size {stateSize} doesn't exist, creating it :{thisStatePath}")
            os.mkdir(thisStatePath)
    maxUsers = int(os.getenv("MAX_USERS", 20))
    modelExpiration = int(os.getenv("MODEL_EXPIRATION_DAYS", 7))