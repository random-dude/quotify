import os
import subprocess
import string
import random
from collections import deque
import markovify
import config
from books import Book

if config.Markov.TTSchoice == "gTTS" : from gtts import gTTS

class Weight:
    """Stores the weight set on the UI and the calculated weight after balancing"""
    UI=1.
    balanced=1.

class Model():
    def __init__(self):
        """create a mixed markov model between multiples books

        Args:
            books (dict): books used in model : {book.Book : weight }
        """
        self.books = {} # will contain {<book.Book> : <Weight> }
        self.model = None
        # the model will remember the last 50 sentences generated to avoid repeating itself
        self.sentences = deque([""], 50)

    def balanceWeights(self, booksInUse):
        """since each book has a different size, bigger books tends to be favoritized when mixed along smaller ones.
        To prevent this behavior, we correct each weight by multiplying it by a bias factor inversely proportionnal to the size of the book:\n
        bias (%) = (allBooks.len - thisBook.len) / allBook.len\n
        weight = weight * bias
        """
        totalBooksLength = sum([b.length for b, _ in booksInUse.items()])
        for book, weight in self.books.items():
            balancedWeight = weight.UI * (totalBooksLength - book.length)/totalBooksLength
            self.books[book].balanced = balancedWeight
        # FIXME debug : print debug info to explore hybrid model weights
        # totalBooksWeights = sum(([w.UI for w in self.books.values()]))
        # totalBalancedWeights = sum(([w.balanced for w in self.books.values()]))
        # for book, weight in self.books.items() : 
        #     weightPercent = int(100 * weight.UI/totalBooksWeights)
        #     balancedWeightPercent = int(100 * weight.balanced/totalBalancedWeights )
        #     lengthPercent = int(100 * book.length/totalBooksLength)
        #     print(f"{book.title} length={lengthPercent}%, weight={weightPercent}%, balancedWeight={balancedWeightPercent}%")

    def update(self):
        """recompute mixed model after adding or removing a book or when a weight is changed
        """
        booksInUse = {b: w for b, w in self.books.items() if w.UI > 0.05}     # filter out the books with weights <5%
        if len(booksInUse) == 0:
            self.model = None
        # if only one book left, the model used will be this book single model
        elif len(booksInUse) == 1:
            lonelyBook = next(iter(booksInUse))
            if not lonelyBook.isSingle : lonelyBook.useHybridModel(False)
            self.model = lonelyBook.model
        else:
        # if multiple books are present, we will switch them to the hybrid model and balance their weight according to their length
            for book in booksInUse.keys():
                if book.isSingle : book.useHybridModel()
            self.balanceWeights(booksInUse)
            self.model = markovify.combine([b.model for b in self.books], [w.balanced for w in self.books.values()])
        print("updated model")
        for book, weight in booksInUse.items():
            print(f'  book {book.title} weight {int(100*weight.balanced)} is using {"single" if book.isSingle else "hybrid"} model')

    def addBook(self, book:    dict):
        """add a book.Book object in the model, based on it's properties (id, title, author, filename, isCurated)

        Args:
            book (dict): as in bookSummary.json : {title:str, author:str, id:str, filename:str, isCurated:bool}
        """
        if book["id"] in [b.id for b in self.books] : return # avoid adding the same book twice
        self.books[Book(book)] = Weight()
        # if the weight is included inside the dict, set the book object weight to it's value 
        if "weight" in book : self.updateWeight(book["id"], book["weight"]/100.)
        print("added book", book["title"], "to model which contains", {b.title: w.UI for b, w in self.books.items()})
        self.update()

    def getBookById(self, id) -> Book:
        """returns the book object from Book class from it's filename

        Args:
            id (str): random alphanumerical string generated when the server starts

        Returns:
            Book object: will be None if the book wasn't loaded
        """
        try:
            book = next(b for b in self.books if b.id == id)
            return book
        except StopIteration:
            print("book not found :", id)
            return None

    def removeBookById(self, id: str):
        """delete a book from the internal model

        Args:
            id (str): random alphanumerical string generated when the server starts
        """
        book = self.getBookById(id)
        if book is not None:
            del(self.books[book])
            print("removed book", book.title, "from model")
            self.update()
        else:
            print("tried to remove non-existing book", id)

    def replaceBookById(self, currentBook: str, newBook: dict):
        self.removeBookById(currentBook)
        self.addBook(newBook)
        print("replaced book", currentBook, "by", newBook["id"])

    def updateWeight(self, id:     str, weight:     float):
        """set the weight of a given book

        Args:
            id (str): random alphanumerical string generated when the server starts
            weight (float): 0~1
        """
        book = self.getBookById(id)
        if book is not None:
            self.books[book].UI = weight
            self.update()
        else:
            print("tried to set weight of non-existing book", id)

    def getSentence(self, maxTries=20, length=280) -> str:
        """tries to get a sentence from the model

        Args:
            length (int, optional): word count. Defaults to 280.
            maxTries (int, optional): number of tries before giving up. Defaults to 10.

        Returns:
            str: generated text if any
        """
        if not self.model:
            return "ajouter un livre pour commencer"
        for _ in range(maxTries):
            text = self.model.make_short_sentence(length)
            if text is not None and text not in self.sentences :
                text = text.replace('"', "").replace("«", "").replace("»", "")
                self.sentences.append(text)
                return f"«{text}»"
        print(f"failed to generate a sentence {maxTries} times in a row")
        return None

    def getActiveBooks(self) -> list:
        """returns a list of dict containing every book loaded and it's weight

        Returns:
            list of dict: {title:str, author:str, filename:str, isCurated:bool, weight:int 0~100} 
        """
        activeBooks = []
        for b, w in self.books.items():
            activeBooks.append({"title": b.title, "author": b.author, "isCurated": b.isCurated,
                                "filename": b.filename, "id":b.id, "weight": int(w.UI*100)})
        return activeBooks

    def readTextToSpeech(self, text: str) -> str:
        """use picoTTS to generate a randomly named wavefile from a given text

        Args:
            text (str): text to synthesize

        Returns:
            str: wav filename
        """
        extension = ".mp3" if config.Markov.TTSchoice == "gTTS" else ".wav"
        filename = "".join(random.choices(string.ascii_lowercase + string.digits, k=20)) + extension
        path = os.path.join(config.Markov.audioPath, filename)
        if config.Markov.TTSchoice == "gTTS" :
            tts = gTTS(text=text, lang="fr", slow=False)
            subprocess.call(f"cd {config.Markov.audioPath} && rm ./*.mp3", shell=True)
            tts.save(path)
        elif config.Markov.TTSchoice == "picoTTS":
            cmd = config.Markov.picoTTSpath + f' --lang="fr-FR" -w {path} "{text}"'
            print(cmd)
            subprocess.call(f"cd {config.Markov.audioPath} && rm ./*.wav", shell=True)
            subprocess.call(cmd, shell=True)
        else :
            print("unknown USE_TTS :", config.Markov.TTSchoice)
        return filename
