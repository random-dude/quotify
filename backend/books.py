import json
import glob
import os
import string
import random
import markovify
import spacy
from config import Markov as config

#TODO : generate and store multiples models based on the state_size

# static variables
scriptPath = os.path.abspath(os.path.dirname(__file__))
corpusPath = scriptPath+"/corpus"
booksSummary = "booksSummary.json"

# this global variable will contain every book found, in the same format as bookSummary
availables = []

# overriding POSifiedText to make use of spacy
nlp = spacy.load("fr_core_news_sm")


class POSifiedText(markovify.Text):
    def word_split(self, sentence):
        return ["::".join((word.orth_, word.pos_)) for word in nlp(sentence)]

    def word_join(self, words):
        sentence = " ".join(word.split("::")[0] for word in words)
        return sentence


def preComputeModel(filename: str, stateSize:int):
    """generate a .JSON file containing the text segmented using spacy

    Args:
        filename (str) : source txt file filename
        stateSize (int) : NLP state size to use
    """
    corpus = open(prefixPath(filename)).read()
    textModel = markovify.Text(corpus, state_size=stateSize)
    JSONmodel = textModel.to_json()
    filename = os.path.join(str(stateSize), getJsonPath(filename))
    with open(prefixPath(filename), "w", encoding="utf-8") as f:
        json.dump(json.loads(JSONmodel), f, ensure_ascii=False, indent=4)
    print("    done pre-computing model for", filename)


def init():
    """parse the books folder to identify usables books, generate pre-computed JSON if not already done,
    update the JSON summary and update the global variable "availables".

    Should be called before using the Book class to avoid missing files.
    """
    global availables
    summary = []
    print("initialising books")

    # open the file summary or create it
    summaryPath = prefixPath(booksSummary)
    try:
        with open(summaryPath, "r", encoding="utf-8") as f:
            summary = json.load(f)
    except IOError:
        print(
            f"  unable to find the book summary {summaryPath}, creating one from scratch...")
        with open(summaryPath, "wt", encoding="utf-8") as f:
            f.write("[]")

    # check that every file listed in the summary still exist and add newly found books :
    txtFiles = glob.glob(prefixPath("*.txt"))
    for txtFile in txtFiles:
        if txtFile not in [prefixPath(b["filename"]) for b in summary]:
            txtFile = os.path.basename(txtFile)
            print("  found the book", txtFile, "that wasn't listed on the summary, adding it")
            summary.append({"filename": txtFile, "title": os.path.splitext(
                txtFile)[0], "author": "unknown", "isCurated": False, "id": getRandomString(20)})
    for book in summary:
        if prefixPath(book["filename"]) not in txtFiles:
            print("  unable to find the book", book["filename"], ", marking it as not curated ")
            book["isCurated"] = False

    # verify that every curated book has it's own pre-computed model
    availables = [b for b in summary if b["isCurated"]]
    for book in availables:
        for stateSize in (config.singleStateSize, config.hybridStateSize):
            filename = os.path.join(str(stateSize), getJsonPath(book["filename"]))
            preComputedJsonPath = prefixPath(getJsonPath(filename))
            if not os.path.exists(preComputedJsonPath):
                print(f'no precomputed model for file {book["filename"]} at state size {stateSize}, computing it...')
                preComputeModel(book["filename"], stateSize)
            # add a random alphanumerical string as ID
        if not "id" in book : book["id"] = getRandomString(20)

    # actually write the summary
    with open(summaryPath, "w", encoding="utf-8") as f:
        json.dump(summary, f, ensure_ascii=False, indent=3)


# one-liner utilities
def getJsonPath(x): return os.path.splitext(x)[0]+".json"
def prefixPath(x): return os.path.join(corpusPath, x)
def getRandomString(x) : return "".join(random.choices(string.ascii_lowercase + string.digits, k=x))


class Book():
    """compute a model on init and can generate text
    """

    def __init__(self, book: dict, isSingle=True):
        """create a model from the pre-segmented .JSON for a given book and store it into self.model

        Args:
            book (dict): using the bookSummary format :
            {"title":"myTitle", "author":"myAuthor", "filename":"myFile.txt", "isCurated": False}
            isSingle (bool, optional) : will use the model generated for the corresponding state size (single or hybrid)
        """
        print("loading model for book ", book["filename"], "...")
        self.isSingle = isSingle
        filename = os.path.join(str(config.singleStateSize),book["filename"]) if isSingle else os.path.join(str(config.hybridStateSize),book["filename"])
        self.setModel(filename)
        for k, v in book.items():  # adds title, author, filename and isCurated
            setattr(self, k, v) # add --ignored-classes=Book to pylint args
        print(f"  loaded model {self.title} of length {self.length}")

    def setModel(self, filename:str) :
        """reads a precomputed model from a .json file and load it into self.model

        Args:
            filename (str): the name of the text file prefixed by the folder corresponding to the state size used
        """
        with open(prefixPath(getJsonPath(filename)), "r", encoding="utf-8") as jsonFile:
           data = json.dumps(json.load(jsonFile))
        self.model = markovify.Text.from_json(data)
        self.length = len(data)

    def useHybridModel(self, isHybrid=True):
        """ reload a precomputed model from a file with the state size corresponding to the specified mode

        Args:
            isHybrid (bool, optional): Uses the hybrid model state size defined in config or the single one. Defaults to True.
        """
        stateSizeFolder = str(config.hybridStateSize) if isHybrid else str(config.singleStateSize)
        filename = os.path.join(stateSizeFolder, self.filename) 
        self.setModel(filename)
        self.isSingle = not isHybrid

    def generateSentence(self, length=280, maxTries=10):
        """tries to get a sentence from the model

        Args:
            length (int, optional): word count. Defaults to 280.
            maxTries (int, optional): number of tries before giving up. Defaults to 10.

        Returns:
            str: generated text if any
        """
        for _ in range(maxTries):
            text = self.model.make_short_sentence(length)
            if text is not None:
                print(text)
                return text
        print(f"failed to generate a sentence {maxTries} times in a row")
