import os
import glob
import warnings
import json
import pytest
import config

def test_validate_corpus_path():
    """ validates every variable from config in debug and production mode"""
    # validate corpus folder path
    assert os.path.isdir(config.Markov.corpusPath)

def test_text_files_exits_in_corpus_folder():
    # verify that there is text files in the corpus folder
    assert len(glob.glob(os.path.join(config.Markov.corpusPath, "*.txt"))) > 0

def test_validate_configuration_entries_type():
    # validate entries type
    assert type(config.API.port) == int
    assert type(config.API.HTTPport) == int
    assert type(config.Markov.booksSummary) == str
    assert type(config.Markov.TTSchoice) == str
    assert config.Markov.TTSchoice in ("picoTTS", "gTTS")
    assert type(config.API.origins) == list
    assert len(config.API.origins) > 0

def test_validate_booksSummary():
    # verify that booksSummary is either readable or inexistent
    bookSummaryPath = os.path.join(config.Markov.corpusPath, config.Markov.booksSummary)
    try : 
        with open(bookSummaryPath, "rt") as f:
            json.load(f)
    except FileNotFoundError :
        warnings.warn("no booksSummary file found, will be generated on the first launch")
    except json.JSONDecodeError : 
        pytest.fail("unreadable booksSummary : "+bookSummaryPath, False)

def test_picotts_is_working():
    """ uses picoTTS to generate a temporary wav file"""
    assert os.path.isfile(config.Markov.picoTTSpath)
    testAudioFile = "testAudio.wav"
    os.system(config.Markov.picoTTSpath + f' --lang="fr-FR" -w {testAudioFile} "this is a test"')
    assert os.path.isfile(testAudioFile)
    os.remove(testAudioFile)
