from fastapi import FastAPI, HTTPException, Response
from fastapi.middleware.cors import CORSMiddleware
from starlette.responses import RedirectResponse
from uuid import uuid4
from datetime import datetime, timedelta
import markov
import books
import config

app = FastAPI()

# define CORS from config.py
app.add_middleware(CORSMiddleware, allow_origins=config.API.origins,
                   allow_credentials=True, allow_methods=["*"], allow_headers=["*"])

# global variables
users = {} # currently loaded users {uuid : UserModel}
availablesBooks = None

def pruneOlderModels():
    global users
    # remove expired models
    expired = [userID for userID in users if users[userID].hasExpired()]
    for userID in expired : del(users[userID])
    # sort userIDs by lastUsed
    sortedIds = [m.userID for m in sorted(users.values(), key=lambda x : x.lastUsed)]
    # remove older models until maxUsers is respected
    while len(users) >= config.Markov.maxUsers : del(users[sortedIds.pop()])

class UserModel() :
    def __init__(self, userID:str) :
        pruneOlderModels()
        self.markovModel = markov.Model()
        self.lastUsed = datetime.now()
        self.userID = userID

    def model(self):
        self.lastUsed = datetime.now()
        return self.markovModel

    def hasExpired(self):
        return datetime.now() > self.lastUsed + timedelta(days=config.Markov.modelExpiration)


@app.get("/")
async def index():
    """redirect to fastAPI swagger interface"""
    return RedirectResponse(url='/docs')


@app.get("/api/{uid}/getSessionData")
async def getAvailablesBooks(uid):
    """initialise the corpus by scanning and precomputing the books
    This operation can take up to several minutes on the first run depending on the number of txt files and precomputed models
    When the response arrive, the frontend will switch from the loading page to the app

    Returns:
        array: array of dicts formatted like booksSummary.json :
               {"title":str, "author": str, "filename":str, isCurated:bool}
    """
    global availablesBooks, users
    modelNeedsReload = True
    if availablesBooks is None:
        books.init()
        availablesBooks = books.availables
    # new user or expired user
    if uid not in users :
        if uid == "newUser":
            uid = str(uuid4())
            print("added new user", uid)
        # returning user
        else : print("added returning user", uid)
        users[uid] = UserModel(uid)
    # user is already loaded
    else : modelNeedsReload = False
    print(len(users), "users currently loaded")
    return {"availablesBooks":availablesBooks, "uid": uid, "modelNeedsReload":modelNeedsReload}


@app.get("/api/{uid}/getCurrentlyUsedBooks")
async def getCurrentlyUsedBooks(uid:str) -> list:
    # since newly created models take some time to populate users, users[uid] may not exists yet upon first call
    if uid in users :
        return users[uid].model().getActiveBooks()
    else : return []


@app.get("/api/{uid}/addBookById/{id}")
async def addBookByID(uid:str, id: str) -> bool:
    try:
        book = next(b for b in availablesBooks if b["id"] == id)
        users[uid].model().addBook(book)
    except StopIteration:
        print("tried to add a non-existent book of id", id)
    return True

@app.post("/api/{uid}/addBookFromDict")
async def addBookFromDict(uid:str, book:dict) -> bool:
    users[uid].model().addBook(book)
    return True


@app.delete("/api/{uid}/removeBookById/{id}")
async def removeBookById(uid:str, id: str) -> bool:
    users[uid].model().removeBookById(id)
    return True


@app.post("/api/{uid}/replaceBookById")
async def replaceBookById(uid:str, data: dict) -> bool:
    """remove the old book and add the new one"""
    try:
        newBook = next(b for b in availablesBooks if b["id"] == data["newBookId"])
        users[uid].model().replaceBookById(data["toReplace"], newBook)
    except StopIteration:
        print(f'unable to replace {data["toReplace"]} by {data["newBookId"]} : {data["newBookId"]} not found')

    return True

@app.get("/api/{uid}/generateSentence")
async def generateSentence(uid:str) -> str:
    return users[uid].model().getSentence()


@app.post("/api/{uid}/updateBookWeight")
async def updateBookWeight(uid:str, data: dict) -> bool:
    """documented in markov.py, convert the frontend value int 0~100 to a float 0~1"""
    if not uid in users : return False
    users[uid].model().updateWeight(data["id"], data["value"]/100)
    return True


@app.get("/api/{uid}/getAudio")
async def textToSpeech(uid:str) -> str:
    audioPath = users[uid].model().readTextToSpeech(users[uid].model().sentences[-1])
    return audioPath
