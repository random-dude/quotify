import pytest
import os
from shutil import copy
import config
import markov

TEST_BOOK_FILENAME = "test-corpus.json"
DUMMY_BOOK = {"title":"dummy title", "author":"dummy author", "isCurated":True, 
              "filename":TEST_BOOK_FILENAME.replace("json", "txt"), "id": "ja2ej2pvfgxnfuunvst7"}
BASE_WEIGHT = 1.

# chdir to the folder of this file
os.chdir(os.path.abspath(os.path.dirname(__file__)))

@pytest.fixture
def model():
    """initialize a model based on a true book pre-generated JSON
    The .json is copied to the prod corpus folder beforehands and removed afterwards.       
    Yields:
        [markov.Model]: fully fonctionnal model with DUMMY_BOOK loaded
    """
    copy(TEST_BOOK_FILENAME, config.Markov.corpusPath)
    model = markov.Model()
    model.addBook(DUMMY_BOOK)
    yield model
    os.remove(os.path.join(config.Markov.corpusPath, TEST_BOOK_FILENAME))
    return

def pass_generateText(model):
    """tests the text generation, to be used each time the model is alterated"""
    sentence = model.getSentence(maxTries=200)
    assert type(sentence) == str
    assert sentence != "ajouter un livre pour commencer"

def test_getActiveBooks(model):
    """returns a list of dicts with weights from 0-100 for the UI"""
    activeBook = DUMMY_BOOK
    activeBook["weight"] = int(BASE_WEIGHT) * 100
    assert model.getActiveBooks()[0] == activeBook

def test_getBookById(model):
    assert isinstance(model.getBookById(DUMMY_BOOK["id"]), markov.Book)

def test_removeBook(model):
    """tests the default sentence returned when there is no book loaded"""
    model.removeBookById(DUMMY_BOOK["id"])
    assert model.books == {}
    assert model.getSentence(maxTries=200) == "ajouter un livre pour commencer"

def test_single_book_uses_single_model(model):
    book = next(iter(model.books))
    assert book.isSingle == True

def test_replaceBook(model):
    """Will test addBook and removeBook at the same time"""
    anotherBook = DUMMY_BOOK
    anotherBook["title"] = "dummy book episode two"
    model.replaceBookById(DUMMY_BOOK["id"], anotherBook)
    newBook = next(iter(model.books))
    assert newBook.title == "dummy book episode two"
    assert newBook.isSingle == True
    pass_generateText(model)

def test_updateWeight(model) :
    model.updateWeight(DUMMY_BOOK["id"], BASE_WEIGHT/2)
    dummyBookObject = model.getBookById(DUMMY_BOOK["id"])
    assert model.books[dummyBookObject].UI == BASE_WEIGHT/2
    pass_generateText(model)

def test_multiple_books(model):
    secondBook = DUMMY_BOOK
    secondBook["title"] = "dummy book, the return"
    secondBook["id"] = "dummy book, the return"
    model.addBook(secondBook)
    for book in model.books: assert book.isSingle == False
    pass_generateText(model)