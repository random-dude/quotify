*** Settings ***
Library          SeleniumLibrary
Library          OperatingSystem
Library          Process
Library          RequestsLibrary
Library    XML
Test Setup       Setup
Test Teardown    Teardown

*** Variables ***
${Browser}                 firefox
${Main Page URL}           http://localhost:3000
${API URL}                 http://localhost:8000

*** Tasks ***
Start server                      Start Process                  ./run    --debug    shell=true
Frontend is running               Wait Until Keyword Succeeds    30    1s    Frontend is Running
API is running                    Wait Until Keyword Succeeds    10    1s    API is Running
Open main Page                    Load Main Page
Tutorial Modal is Shown           Wait Until Page Contains    bienvenue    10s
Skip the tutorial                 Skip tutorial
Open the library                  Open Library
Select a random book              Select Random Book
Generate a quote                  Generate Quote
Swap the book for another one     Swap Book
Generate a new quote              Generate Quote
Add a second book                 Add a Random Book
Two books are loaded              Element Count Should Be        class:book    2
Modify book weight                Decrease Random Book Weight
Generate a quote from two books   Generate Quote
Remove a random book              Remove a Random Book
Only one book left                Element Count Should Be        class:book    1
Close browser                     Close Browser
Terminate server                  Terminate All Processes


*** Keywords ***

Setup
    Create Directory            .selenium-screenshots
    Set Screenshot Directory    .selenium-screenshots

Frontend is Running
    GET  ${Main Page URL}
    Status Should Be  200

API is Running
    GET  ${API URL}
    Status Should Be  200

Teardown
    Remove Directory    .selenium-screenshots    recursive=True
    Remove Files        geckodriver-*.log

Load Main Page
    Open Browser                ${Main Page URL}    ${Browser}
    Maximize Browser Window
    Page Should Contain         initialisation de la bibliothèque...
    
Skip tutorial
    Click Element               xpath://p[text()='passer']
    Page Should Contain         choisir un livre dans la bibliothèque pour commencer
    Page Should Not Contain     bienvenue
Open Library
    Click Element               css:[data-bs-target="#booksLibraryModal"]
    Wait Until Page Contains    Librairie

Select Random Book
    ${random book}=                      Select Random Element    css:.list-group-item
    Click Element                        ${random book}
    # for unknown reasons, modal won't close by itself when piloted by geckodriver
    Execute Javascript                   document.getElementById("booksLibraryModal").click()
    Wait Until Element Is Not Visible    id:booksLibraryModal
    Wait Until Page Does Not Contain     calcul du modèle en cours...    timeout=10s
    Page Should Contain                  pour génerer une citation

Add a Random Book
    Open Library
    Select Random Book

Swap Book
    Click Element               class:book-title
    Wait Until Page Contains    Librairie
    Select Random Book

Generate Quote
    Wait Until Page Does Not Contain        calcul du modèle en cours    10s
    Click Element                           css:[title="écouter une prophétie"]
    Wait Until Element Change               class:main-text

Decrease Random Book Weight
    ${random book}=  Select Random Element    class:adjust-weight      red
    Mouse Over        ${random book}
    # since Simulate Event needs an event handler attached, we have to dispatch the wheel evt manually
    Execute Javascript   const e = document.createEvent('MouseEvents'); e.initEvent('wheel',true,true); e.deltaY=-20; document.getElementById("test-selected").dispatchEvent(e);
    Mouse Out    ${random book}
    Sleep    200ms  # gives time for the model adjust itself

Remove a Random Book
    ${random book}=                               Select Random Element    css:.close-book svg
    Click Element                                 ${random book}
    # this will fail while the element is effectively removed from the DOM. Why ?
    # Wait Until Page Does Not Contain Element      ${random book}     
    Sleep                                         1s

Select Random Element
    # will return an element selected at random amongst the elements corresponding to the locator
    # if ${background color} is provided, the element will be colored
    [Arguments]      ${locator}         ${background color}=None
    Wait Until Page Contains Element    ${locator}
    ${items} =       Get WebElements    ${locator}
    ${random item}=  Evaluate           random.choice($items)  random
    IF    "${background color}" != None
        Run Keyword And Ignore Error    Unmark Selected Items
        Assign Id To Element    ${random item}    test-selected
        Execute Javascript  document.getElementById("test-selected").style.backgroundColor="${background color}"
    END
    [Return]         ${random item}

Unmark Selected Items
    # will fail if no elements with id #test-selected exists
    Execute Javascript      document.getElementById("test-selected").removeAttribute("id")

Wait Until Element Change
    [Arguments]                    ${locator}       ${retry}=20    ${interval}=250ms
    ${old element}=                Get WebElement   ${locator}
    Wait Until Keyword Succeeds    ${retry}         ${interval}    Element Does Not Match    ${locator}     ${old element}

Element Does Not Match
    [Arguments]           ${locator}           ${web element}
    ${actual element}=    Get WebElement       ${locator}
    Should Not Be Equal   ${actual element}    ${web element}

Element Count Should Be
    [Arguments]           ${locator}                    ${expected element count}
    ${element count}=     SeleniumLibrary.Get Element Count             ${locator}
    Should Be Equal As Integers    ${element count}    ${expected element count}