#!/bin/bash

source configurations/deploy-aezuri
DEPLOY_FOLDER="${DEPLOY_FOLDER:=deploy-build}"

backend=""
frontend=""
setEnvironnement=""
copy="yep"

while test $# -gt 0; do
  case "$1" in
    -h|--help)
      echo "deploy - deploys the frontend and/or backend on a remote (Apache) server"
      echo " "
      echo "parameters like host, port, paths... can be defined via environnement variables or by editing this script"
      echo " "
      echo "./deploy [-f/--frontend/-b/--backend]"
      echo " "
      echo "options:"
      echo "-h, --help                show brief help"
      echo "-f, --frontend, front     builds the frontend and sends it to the specified remote path"
      echo "-b, --backend, back       sends the FastAPI backend to the specified remote path"
      echo "-ncp, --no-copy, ncp      build without copying anything to the remote server"
      echo "-env, --set-environnement set the environnement variables on the remote server (ignored when -ncp is used)"
      echo " "
      echo "examples :"
      echo "./deploy                 # will deploy both frontend and backend"
      echo "./deploy --no-copy       # will build the backend and frontend into \$DEPLOY_FOLDER without sending them to the remote server"
      echo "./deploy --backend       # will deploy the backend only on the server"
      echo " "
      exit 0
      ;;
    --frontend | -f | front)
      frontend="yep"
      shift
      ;;
    --backend | -b | back)
      backend="yep"
      shift
      ;;
    --no-copy | -ncp | ncp)
      copy=""
      shift
      ;;
    --set-environnement | -env | env)
      setEnvironnement="yep"
      shift
      ;;
    *)
      break
      ;;
  esac
done

if [ -z "$frontend" ] && [ -z "$backend" ]; then
   echo "nothing specified for backend nor frontend, building both by default"
   backend="yep"
   frontend="yep"
fi

if [ -d "./$DEPLOY_FOLDER" ]; then 
   echo "cleaning $DEPLOY_FOLDER from previous builds"
   rm -r $DEPLOY_FOLDER/*
else
   echo "building into $DEPLOY_FOLDER"
   mkdir $DEPLOY_FOLDER 
fi

mkdir $DEPLOY_FOLDER/frontend $DEPLOY_FOLDER/backend


if [ -z "$backend" ]; then
   echo "   skipping backend..."
else

   echo "preparing backend for deployment :"
   cd backend

   echo "   copying python files"
   cp ./*.py ../$DEPLOY_FOLDER/backend

   echo "   freezing pip requirements"
   source .env/bin/activate && pip freeze > ../$DEPLOY_FOLDER/backend/requirements.txt
   deactivate

   echo "   copying the environnement variables script"
   cp ../configurations/deploy-aezuri ../$DEPLOY_FOLDER/backend/source-environnement

   echo "   creating WSGI converter"
   echo "from a2wsgi import ASGIMiddleware
from app import app
WSGIapp = ASGIMiddleware(app)">../$DEPLOY_FOLDER/backend/WSGIapp.py

   echo "   copying .json models..."
   mkdir -p "../$DEPLOY_FOLDER/backend/corpus"
   for d in corpus/*/ ; do
      mkdir "../$DEPLOY_FOLDER/backend/$d"
      cp $d/*.json ../$DEPLOY_FOLDER/backend/$d
   done
   echo "   copying bookSummary..."
   cp corpus/bookSummary.json ../$DEPLOY_FOLDER/backend/corpus/

   echo "   creating empty .txt files..."
   for filename in corpus/*.txt; do
      touch "../$DEPLOY_FOLDER/backend/$filename"
    done

   if [ "$USE_TTS" == "picoTTS" ]; then
      echo "   copying picoTTS binaries"
      mkdir -p "../$DEPLOY_FOLDER/picotts"
      cp -r ./picotts/dist "../$DEPLOY_FOLDER/picotts/"
   fi

   echo "done building backend in $DEPLOY_FOLDER/backend"
   echo " "
   cd ..
fi



if [ -z "$frontend" ]; then
   echo "   skipping frontend..."
else
   echo "preparing frontend for deployment..."
   if [ "$API_PORT" -eq "80" ]; then 
         API_URL="http://$API_HOST/api"
      else
         API_URL="http://$API_HOST:$API_PORT/api"
   fi
   cd frontend
   export BUILD_PATH=../$DEPLOY_FOLDER/frontend REACT_APP_FRONTEND_AUDIO_PATH="$FRONTEND_AUDIO_PATH" REACT_APP_API_ENDPOINT=$API_URL
   echo "   building frontend..."
   npm run build
   cd ..
fi

if [ -z "$copy" ]; then
   echo " "
   echo "----------------DONE------------------"
   echo "    the build is ready to deploy "
   echo "       from $DEPLOY_FOLDER "
   echo "--------------------------------------"
   exit 0
else
   echo ""
   echo "deploying to remote server $REMOTE_HOST via SSH..."
   if [ ! -z "$frontend" ]; then
      echo "   deploying frontend"
      echo "scp -rp -P $SSH_PORT $DEPLOY_FOLDER/frontend/* $SSH_USER@$REMOTE_HOST:$REMOTE_FRONTEND_PATH/"
      scp -rp -P $SSH_PORT $DEPLOY_FOLDER/frontend/* $SSH_USER@$REMOTE_HOST:$REMOTE_FRONTEND_PATH/
   fi
   if [ ! -z "$backend" ]; then
      echo "   deploying backend"
      echo "scp -rp -P $SSH_PORT $DEPLOY_FOLDER/backend/* $SSH_USER@$REMOTE_HOST:$REMOTE_BACKEND_PATH"
      scp -rp -P $SSH_PORT $DEPLOY_FOLDER/backend/* $SSH_USER@$REMOTE_HOST:$REMOTE_BACKEND_PATH
      if [ ! -z "$setEnvironnement" ]; then
         echo "      setting up the environnement variables"
         echo "ssh -p $SSH_PORT $SSH_USER@$REMOTE_HOST 'cd $REMOTE_BACKEND_PATH; source ./source-environnement' "
         ssh -p $SSH_PORT $SSH_USER@$REMOTE_HOST 'cd $REMOTE_BACKEND_PATH; source ./source-environnement'
      fi
   echo " "
   echo "-------------------------DONE---------------------------"
   echo "     the build has been deployed to $REMOTE_HOST "
   echo "              from $DEPLOY_FOLDER "
   echo "--------------------------------------------------------"
   exit 0
   fi
fi